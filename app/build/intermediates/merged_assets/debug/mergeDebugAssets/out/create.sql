CREATE TABLE [LoanStatus] (
	[LnStatus] Integer PRIMARY KEY,
	[StName] nvarchar(20) 
);
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (0, 'Current');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (1, 'Closed');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (2, 'Late1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (3, 'Late2');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (4, 'NIBL1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (5, 'NIBL2');

CREATE TABLE [OrgCategories] (
	[OrgCategoryID] nchar(2) PRIMARY KEY,
	[OrgCategoryName] nvarchar(255),
	[Status] tinyint
);

CREATE TABLE [Projects] (
	[ProjectCode] nchar(3) PRIMARY KEY,
	[ProjectName] nvarchar(255),
	[OrgCategory] nchar(2),
	[Status] tinyint
);

CREATE TABLE [POList] (
	[CONo] nchar(8) PRIMARY KEY,
	[COName] nvarchar(255),
	[SessionNo] Integer NULL,
	[OpenDate] datetime NULL,
	[OpeningBal] Integer NULL,
	[Password] nvarchar(10) NULL,
	[EMethod] smallint NULL DEFAULT 0,
	[CashInHand] Integer NULL,
	[EnteredBy] nvarchar(10) NULL,
	[SYSDATE] datetime NULL,
	[SYSTIME] datetime NULL,
	[AltCOName] nvarchar(255),
	[BranchCode] nchar(4),
	[BranchName] nvarchar(255),
	[BODate] datetime NULL,
	[BOStatus] Integer,
	[SMSSupport] Integer DEFAULT 0,
	[ProjectCode] Integer DEFAULT 0,
	[TransSlNo] int NULL DEFAULT 0,
	[LastSyncTime] datetime
);
DELETE FROM POList;
INSERT INTO POList([CONo], [COName], [SessionNo], [OpenDate], [OpeningBal], [Password],
                    [EMethod], [CashInHand], [EnteredBy], [AltCOName], [BranchCode], [BranchName],
                     [BODate], [BOStatus], [SMSSupport], [ProjectCode], [TransSlNo], [LastSyncTime])
	VALUES('00000000', 'Nobody', 0, '2015-02-03', 0, '', 0, 0, '00000000', 'Nobody', '0000', '', '2015-02-03', 1, 0, 0, 0, '2015-01-01 12:00:00');

CREATE TABLE [COList] (
	[CONo] nchar(8) PRIMARY KEY,
	[COName] nvarchar(255),
	[LastPOSyncTime] datetime
);

CREATE TABLE [VOList] (
	[OrgNo] Long(8) PRIMARY KEY,
	[OrgName] nvarchar(255),
	[OrgCategory] nchar(2),
	[MemSexID] int,
	[SavColcOption] int,
	[LoanColcOption] int,
	[SavColcDate] datetime,
	[LoanColcDate] datetime,
	[TargetDate] datetime,
	[Period] int,
	[CONo] nchar(8),
	[PeriodStart] datetime,
	[PeriodEnd] datetime,
	[NextTargetDate] datetime,
	[TerritoryName] nvarchar(255),
	[UpdatedAt] datetime,
	[ProjectCode] nchar,
	[BranchCode] nchar
);

CREATE TABLE [LoanProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	[IntrRate] real,
	[IntCalcMethod] tinyint,
	[IntRateIntrvlDays] int,
	[IntrFactorLoan] real,
	PRIMARY KEY ([ProductNo]) 
);

CREATE TABLE [SavingsProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	PRIMARY KEY ([ProductNo])
);
	
CREATE TABLE [SavingsInterestPolicy] (
	[ProductNo] nchar(3),
	[OrgCategory] nchar(2),
	[ProjectCode] nchar(3),
	[MinSavBalance] int,
	[MaxSavBalance] int,
	[InterestRate] real,
	PRIMARY  KEY ([ProductNo], [OrgCategory], [ProjectCode])
);
		
CREATE TABLE [CMembers] (
	[ProjectCode] nchar(3),
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[MemberName] nvarchar(255),
	[SavBalan] Integer,
	[SavPayable] Integer,
	[CalcIntrAmt] real,
	[TargetAmtSav] Integer,
	[ColcDate] datetime,
	[ReceAmt] Integer DEFAULT 0,
	[AdjAmt] Integer,
	[SB] Integer,
	[SIA] Integer,
	[SavingsRealized] Integer,
	[memSexID] Integer,
	[NationalIDNo] nvarchar(17),
	[PhoneNo] nvarchar(20),
	[WalletNo] nvarchar(20),
	[AdmissionDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [CMReceAmt] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ReceAmt] Integer DEFAULT 0,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [CLoans] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] Integer,
	[LoanSlNo] Integer,
	[ProductNo] nchar(3),
	[IntrFactorLoan] real,
	[PrincipalAmt] Integer,
	[SchmCode] nchar(3),
	[InstlAmtLoan] Integer,
	[DisbDate] datetime,
	[LnStatus] Integer,
	[PrincipalDue] Integer,
	[InterestDue] Integer,		
	[TotalDue] Integer,
	[TargetAmtLoan] Integer,
	[TotalReld] Integer,
	[Overdue] Integer,
	[BufferIntrAmt] real,
	[ReceAmt] Integer DEFAULT 0,
	[IAB] Integer,
	[ODB] Integer,
	[TRB] Integer,
	[LB] Integer,
	[PDB] Integer,
	[IDB] Integer,
	[InstlPassed] Integer,
	[OldInterestDue] real,
	[ProductSymbol] nchar(3),
	[LoanRealized] Integer,
	[ColcDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo], [LoanNo])
);

CREATE TABLE [CLReceAmt] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[LoanNo] Integer,
	[ReceAmt] Integer DEFAULT 0,
	PRIMARY KEY ([OrgNo], [OrgMemNo], [LoanNo])
);

CREATE TABLE [Transact] (
	_id INTEGER PRIMARY KEY,
	[TransSlNo] int NULL DEFAULT 0,
	[ColcID] int NULL DEFAULT 0,
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[ColcAmt] Integer,
	[SavAdj] Integer,
	[ColcDate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[ColcFor] nchar(1),
	[ProductSymbol] nchar(3),
    [Balance] Integer DEFAULT 0,
    [SMSStatus] Integer DEFAULT 0
);

CREATE TABLE [WPTransact] (
	_id  INTEGER PRIMARY KEY,
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[ColcAmt] Integer,
	[ColcDate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[ColcFor] nchar(1),
	[Verified] nchar(1) DEFAULT '0'
);

CREATE TABLE [TransTrail] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[Tranamount] Integer,
	[ColcDate] datetime NOT NULL,
	[Transno] nchar(13),
	[TrxType] nchar(13),
	[ColcFor] nchar(1),
	[UpdatedAt] datetime,
	PRIMARY KEY ([Transno])
);
CREATE INDEX TransTrail_ColcDate ON TransTrail(ColcDate);

CREATE TABLE [TargetTrail] (
	_id INTEGER PRIMARY KEY,
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[LnStatus] Integer,
	[TargetDate] datetime,
	[TargetAmount] Integer,
	[ExpColcDate] datetime
);
CREATE INDEX TargetTrail_TargetDate ON TargetTrail(TargetDate);

CREATE TABLE [GoodLoans] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[MaxAmt] int,
	[Taken] nchar(1),
	[UpdatedAt] datetime NULL,
	[_Month] Integer,
	[_Year] Integer,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);


-- new Table For Pass book Check info
CREATE TABLE [VOVisit](
	[VisitDate] datetime NOT NULL DEFAULT (Date(current_timestamp, 'localtime')),
	[OrgNo] Long(8),
	[TotalMemb] int,
	[PrsentMemb] int,
	[TotalBorrower] int,
	[CurrentBorrower] int,
	[PBComment] nvarchar,
	[LastUpdate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[DataSent] int DEFAULT 0,
	PRIMARY KEY ([VisitDate], [OrgNo])
);

-- new Table For Pass book Check info
CREATE TABLE [VOBCheck](
    _id INTEGER PRIMARY KEY,
    [AutoGen] int Default 0,
	[VisitDate] datetime NOT NULL DEFAULT (Date(current_timestamp, 'localtime')),
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProductSymbol] nchar(3),
	[LoanNo] int DEFAULT 0,
	[Balance] int DEFAULT 0,
	[PassBook] int NULL,
	[CLoc] tinyint DEFAULT 0,
	[lat] double default 0,
	[lon] double default 0,
	[LastUpdate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[DataSent] int DEFAULT 0
);

-- new Table For Collection Sheet Check info
CREATE TABLE [CSCheckInfo](
    [ID] int,
	[ColcDate] datetime,
	[CONo] nchar(8),
	[OrgNo] Long(8),
	[TotCurrBorrower] int,
	[MisInstlNumb] int,
	[MisInslAmnt] Int,
	[ODReal] Int
);

-- new Table For Collection Sheet Check info
CREATE TABLE [CSCheckInfoEv](
    [ID] int,
	[ColcDate] datetime,
	[CONo] nchar(8),
	[OrgNo] Long(8),
	[ODCurNo] int,
	[ODCurAmt] Integer,
	[ODLatNo] int,
	[ODLatAmt] Integer,
	[ODNiblAmt] int,
	[ODTotAmt] Integer
);

-- new Table For Loan Disbursment info
CREATE TABLE [LoanDisbursInfo](
    [ID] int,
	[DisburseDate] datetime NOT NULL,
	[LoanType] nchar(1),
	[ToNumb] int,
	[ToAmt] Integer,
	[CumilitNo] int,
	[CumilitAmt] Integer
);

-- Followup
CREATE TABLE [Folloup](
    _id INTEGER PRIMARY KEY,
	[FDate] datetime NOT NULL,
	[FCType] smallint,
	[Comment] nvarchar,
	[DataSent] smallint DEFAULT 0
);

CREATE TABLE [FCommentType](
	[FCType] smallint PRIMARY KEY,
	[CDesc] nvarchar
);
INSERT INTO [FCommentType] ([FCType], [CDesc]) VALUES (1, 'OD Loan Inspection');
INSERT INTO [FCommentType] ([FCType], [CDesc]) VALUES (2, 'New Admission');
INSERT INTO [FCommentType] ([FCType], [CDesc]) VALUES (3, 'New Scheme Loan Pre-disbursement Inspection');
INSERT INTO [FCommentType] ([FCType], [CDesc]) VALUES (4, 'Old Scheme Loan Pre-disbursement Inspection');
INSERT INTO [FCommentType] ([FCType], [CDesc]) VALUES (5, 'Saving Withdrawal');

CREATE TABLE [TrxMapping] (
	[TrxShortType] nchar(2),
	[TrxTypeValue] int,
	[ColcFor] nchar(2),
    PRIMARY KEY ([TrxShortType], [TrxTypeValue], [ColcFor])
);
