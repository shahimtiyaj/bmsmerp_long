CREATE TABLE [TargetTrail] (
	_id INTEGER PRIMARY KEY,
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[LnStatus] Integer,
	[TargetDate] datetime,
	[TargetAmount] Integer,
	[ExpColcDate] datetime
);

CREATE INDEX TargetTrail_TargetDate ON TargetTrail(TargetDate);