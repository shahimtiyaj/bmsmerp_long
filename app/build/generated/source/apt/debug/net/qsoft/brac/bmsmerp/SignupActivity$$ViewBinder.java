// Generated code from Butter Knife. Do not modify!
package net.qsoft.brac.bmsmerp;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class SignupActivity$$ViewBinder<T extends SignupActivity> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131296471, "field '_nameText'");
    target._nameText = finder.castView(view, 2131296471, "field '_nameText'");
    view = finder.findRequiredView(source, 2131296469, "field '_emailText'");
    target._emailText = finder.castView(view, 2131296469, "field '_emailText'");
    view = finder.findRequiredView(source, 2131296470, "field '_mobileText'");
    target._mobileText = finder.castView(view, 2131296470, "field '_mobileText'");
    view = finder.findRequiredView(source, 2131296349, "field '_signupButton'");
    target._signupButton = finder.castView(view, 2131296349, "field '_signupButton'");
    view = finder.findRequiredView(source, 2131296501, "field '_loginLink'");
    target._loginLink = finder.castView(view, 2131296501, "field '_loginLink'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends SignupActivity> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target._nameText = null;
      target._emailText = null;
      target._mobileText = null;
      target._signupButton = null;
      target._loginLink = null;
    }
  }
}
