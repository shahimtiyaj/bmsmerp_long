// Generated code from Butter Knife. Do not modify!
package net.qsoft.brac.bmsmerp;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class FollowupActivity$$ViewBinder<T extends FollowupActivity> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131296809, "field 'textODL_L'");
    target.textODL_L = finder.castView(view, 2131296809, "field 'textODL_L'");
    view = finder.findRequiredView(source, 2131296810, "field 'textODL_V'");
    target.textODL_V = finder.castView(view, 2131296810, "field 'textODL_V'");
    view = finder.findRequiredView(source, 2131296802, "field 'textNewAd_L'");
    target.textNewAd_L = finder.castView(view, 2131296802, "field 'textNewAd_L'");
    view = finder.findRequiredView(source, 2131296803, "field 'textNewAd_V'");
    target.textNewAd_V = finder.castView(view, 2131296803, "field 'textNewAd_V'");
    view = finder.findRequiredView(source, 2131296804, "field 'textNewSCM_L'");
    target.textNewSCM_L = finder.castView(view, 2131296804, "field 'textNewSCM_L'");
    view = finder.findRequiredView(source, 2131296805, "field 'textNewSCM_V'");
    target.textNewSCM_V = finder.castView(view, 2131296805, "field 'textNewSCM_V'");
    view = finder.findRequiredView(source, 2131296816, "field 'textOldSCM_L'");
    target.textOldSCM_L = finder.castView(view, 2131296816, "field 'textOldSCM_L'");
    view = finder.findRequiredView(source, 2131296817, "field 'textOldSCM_V'");
    target.textOldSCM_V = finder.castView(view, 2131296817, "field 'textOldSCM_V'");
    view = finder.findRequiredView(source, 2131296844, "field 'textSBW_L'");
    target.textSBW_L = finder.castView(view, 2131296844, "field 'textSBW_L'");
    view = finder.findRequiredView(source, 2131296845, "field 'textSBW_V'");
    target.textSBW_V = finder.castView(view, 2131296845, "field 'textSBW_V'");
    view = finder.findRequiredView(source, 2131296361, "field 'cancelButton'");
    target.cancelButton = finder.castView(view, 2131296361, "field 'cancelButton'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends FollowupActivity> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.textODL_L = null;
      target.textODL_V = null;
      target.textNewAd_L = null;
      target.textNewAd_V = null;
      target.textNewSCM_L = null;
      target.textNewSCM_V = null;
      target.textOldSCM_L = null;
      target.textOldSCM_V = null;
      target.textSBW_L = null;
      target.textSBW_V = null;
      target.cancelButton = null;
    }
  }
}
