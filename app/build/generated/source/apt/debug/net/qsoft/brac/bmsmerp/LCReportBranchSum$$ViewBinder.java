// Generated code from Butter Knife. Do not modify!
package net.qsoft.brac.bmsmerp;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class LCReportBranchSum$$ViewBinder<T extends LCReportBranchSum> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131296839, "field 'mRPTHead'");
    target.mRPTHead = finder.castView(view, 2131296839, "field 'mRPTHead'");
    view = finder.findRequiredView(source, 2131296734, "field 'mDateFrom'");
    target.mDateFrom = finder.castView(view, 2131296734, "field 'mDateFrom'");
    view = finder.findRequiredView(source, 2131296735, "field 'mDateTo'");
    target.mDateTo = finder.castView(view, 2131296735, "field 'mDateTo'");
    view = finder.findRequiredView(source, 2131296835, "field 'mPONoH'");
    target.mPONoH = finder.castView(view, 2131296835, "field 'mPONoH'");
    view = finder.findRequiredView(source, 2131296574, "field 'cmdOK'");
    target.cmdOK = finder.castView(view, 2131296574, "field 'cmdOK'");
    view = finder.findRequiredView(source, 2131296510, "field 'lv'");
    target.lv = finder.castView(view, 2131296510, "field 'lv'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends LCReportBranchSum> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target.mRPTHead = null;
      target.mDateFrom = null;
      target.mDateTo = null;
      target.mPONoH = null;
      target.cmdOK = null;
      target.lv = null;
    }
  }
}
