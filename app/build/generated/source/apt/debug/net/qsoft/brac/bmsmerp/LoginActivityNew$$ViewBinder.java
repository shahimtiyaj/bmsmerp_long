// Generated code from Butter Knife. Do not modify!
package net.qsoft.brac.bmsmerp;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class LoginActivityNew$$ViewBinder<T extends LoginActivityNew> implements ViewBinder<T> {
  @Override
  public Unbinder bind(final Finder finder, final T target, Object source) {
    InnerUnbinder unbinder = createUnbinder(target);
    View view;
    view = finder.findRequiredView(source, 2131296472, "field '_passwordText'");
    target._passwordText = finder.castView(view, 2131296472, "field '_passwordText'");
    view = finder.findRequiredView(source, 2131296347, "field '_loginButton'");
    target._loginButton = finder.castView(view, 2131296347, "field '_loginButton'");
    view = finder.findRequiredView(source, 2131296502, "field '_signupLink'");
    target._signupLink = finder.castView(view, 2131296502, "field '_signupLink'");
    return unbinder;
  }

  protected InnerUnbinder<T> createUnbinder(T target) {
    return new InnerUnbinder(target);
  }

  protected static class InnerUnbinder<T extends LoginActivityNew> implements Unbinder {
    private T target;

    protected InnerUnbinder(T target) {
      this.target = target;
    }

    @Override
    public final void unbind() {
      if (target == null) throw new IllegalStateException("Bindings already cleared.");
      unbind(target);
      target = null;
    }

    protected void unbind(T target) {
      target._passwordText = null;
      target._loginButton = null;
      target._signupLink = null;
    }
  }
}
