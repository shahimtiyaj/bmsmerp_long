/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.qsoft.brac.bmsm;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "net.qsoft.brac.bmsmerp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "0.0.1";
}
