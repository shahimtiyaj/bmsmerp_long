ALTER TABLE [VOList] ADD COLUMN [PeriodStart] DateTime;
ALTER TABLE [VOList] ADD COLUMN [PeriodEnd] DateTime;

CREATE TABLE [CMReceAmt] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[ReceAmt] Integer DEFAULT 0,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [CLReceAmt] (
	[OrgNo] Long(8),
	[OrgMemNo] Long(10),
	[LoanNo] Integer,
	[ReceAmt] Integer DEFAULT 0,
	PRIMARY KEY ([OrgNo], [OrgMemNo], [LoanNo])
);
