package net.qsoft.brac.bmsmerp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;

import net.qsoft.brac.bmsmerp.data.LGPModel;

import java.util.ArrayList;

public class LoanInGracePeriodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<LGPModel> loanGracePeriodArrayList;
    private Context context;

    public LoanInGracePeriodAdapter(Context context, ArrayList<LGPModel> loanGraceArrayList) {
        this.context = context;
        this.loanGracePeriodArrayList = loanGraceArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loan_grace_period, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_HEADER) {
            //Inflating header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.total_amount, parent, false);
            return new HeaderViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_footer_layout, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            // headerHolder.headerTitle.setText("Header View");

        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.footerText.setText("Footer View");

        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder viewHolder = (ItemViewHolder) holder;

            final LGPModel row = (LGPModel) loanGracePeriodArrayList.get(position);
            if (loanGracePeriodArrayList.size() == (position + 1)) {
                try {

                    viewHolder.si_no.setText("");
                    viewHolder.st_name.setText("");
                    viewHolder.vo_code.setText("");
                    viewHolder.member_code.setText("");
                    viewHolder.member_name.setText("");
                    viewHolder.loan_number.setText("");
                    viewHolder.disb_date.setText("Total: ");
                    viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                    viewHolder.first_vo_coll_after_disb.setText("");
                    viewHolder.actual_target_Date.setText("");
                    viewHolder.disb_date.setTypeface(Typeface.DEFAULT_BOLD);
                    viewHolder.target_ammount.setTypeface(Typeface.DEFAULT_BOLD);
                    viewHolder.itemView.setBackgroundResource(R.drawable.footer_border);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                viewHolder.si_no.setText(String.format("%d", position + 1));
                viewHolder.st_name.setText(row.getTerritoryName());
                viewHolder.vo_code.setText(row.getOrgNo());
                viewHolder.member_code.setText(row.getOrgMemNo());
                viewHolder.member_name.setText(row.getMemberName());
                viewHolder.loan_number.setText(row.getLoanNo());
                viewHolder.disb_date.setText(row.getDisbDate("dd-MMM-yyyy"));
                viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                viewHolder.first_vo_coll_after_disb.setText(row.getCollDateAfterDisb("dd-MMM-yyyy"));
                viewHolder.actual_target_Date.setText(row.getActualTargetDate("dd-MMM-yyyy"));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == loanGracePeriodArrayList.size() + 1) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;

        }
    }

    @Override
    public int getItemCount() {
        return loanGracePeriodArrayList.size();
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTitle;

        public HeaderViewHolder(View view) {
            super(view);
            headerTitle = (TextView) view.findViewById(R.id.totalSum);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView si_no, st_name, vo_code, member_code, member_name, loan_number,
                disb_date, target_ammount, first_vo_coll_after_disb, actual_target_Date;

        public ItemViewHolder(View itemView) {
            super(itemView);

            si_no = itemView.findViewById(R.id.LoanInGracetextSIno);
            st_name = itemView.findViewById(R.id.LoanInGracetextSTName);
            vo_code = itemView.findViewById(R.id.LoanInGracetextVOCode);
            member_code = itemView.findViewById(R.id.LoanInGracetextMemberCode);
            member_name = itemView.findViewById(R.id.LoanInGracetextMemberName);
            loan_number = itemView.findViewById(R.id.LoanInGracetextLoanNumber);
            disb_date = itemView.findViewById(R.id.LoanInGracetextDisbdate);
            target_ammount = itemView.findViewById(R.id.LoanInGracetextTargetAmount);
            first_vo_coll_after_disb = itemView.findViewById(R.id.LoanInGrace1stVOCollDateAftDisb);
            actual_target_Date = itemView.findViewById(R.id.LoanInGraceActualTargetDate);

        }
    }
}
