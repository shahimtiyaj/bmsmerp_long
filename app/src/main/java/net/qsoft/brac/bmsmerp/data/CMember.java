package net.qsoft.brac.bmsmerp.data;

import java.util.Date;

public class CMember {
    private static CMember _lastCM = null;

	public static CMember get_lastCM() {
		return _lastCM;
	}

	public static void set_lastCM(CMember _lastCM) {
		CMember._lastCM = _lastCM;
	}

	String _ProjectCode;
	Long _OrgNo;
	Long _OrgMemNo;
	String _MemberName;	
	Integer _SavBalan;
	Integer _SavPayable;
	double _CalcIntrAmt;
	Integer _TargetAmtSav;
	Date _ColcDate;
	Integer _ReceAmt;
	Integer _AdjAmt;
	Integer _SB;
	Integer _SIA;
	Integer _SavingsRealized;
	Integer _memSexID;
	String _NationalIDNo;
	String _PhoneNo;
	String _WalletNo;
	Date _AdmissionDate;

	public CMember(String _ProjectCode, Long _OrgNo, Long _OrgMemNo,
			String _MemberName, Integer _SavBalan, Integer _SavPayable,
			double _CalcIntrAmt, Integer _TargetAmtSav, Date _ColcDate,
			Integer _ReceAmt, Integer _AdjAmt, Integer _SB, Integer _SIA,
			Integer _SavingsRealized, Integer _memSexID, String _NationalIDNo,
			String _PhoneNo, String _WalletNo, Date _AdmissionDate ) {
		super();
		this._ProjectCode = _ProjectCode;
		this._OrgNo = _OrgNo;
		this._OrgMemNo = _OrgMemNo;
		this._MemberName = _MemberName;
		this._SavBalan = _SavBalan;
		this._SavPayable = _SavPayable;
		this._CalcIntrAmt = _CalcIntrAmt;
		this._TargetAmtSav = _TargetAmtSav;
		this._ColcDate = _ColcDate;
		this._ReceAmt = _ReceAmt;
		this._AdjAmt = _AdjAmt;
		this._SB = _SB;
		this._SIA = _SIA;
		this._SavingsRealized = _SavingsRealized;
		this._memSexID = _memSexID;
		this._NationalIDNo =  _NationalIDNo;
		this._PhoneNo = _PhoneNo;
		this._WalletNo = _WalletNo;
		this._AdmissionDate = _AdmissionDate;
	}

	public Integer get_memSexID() {
		return _memSexID;
	}

	public String get_NationalIDNo() {
		return _NationalIDNo;
	}

	public String get_PhoneNo() {
		return _PhoneNo;
	}

	public String get_WalletNo() {
		return _WalletNo;
	}

	public Date get_AdmissionDate() {
		return _AdmissionDate;
	}

	/**
	 * @return the _SavingsRealized
	 */
	public final Integer get_SavingsRealized() {
		return _SavingsRealized;
	}

	/**
	 * @return the _ReceAmt
	 */
	public final Integer get_ReceAmt() {
		return _ReceAmt;
	}


	/**
	 * @param _ReceAmt the _ReceAmt to set
	 */
	public final void set_ReceAmt(Integer _ReceAmt) {
		this._ReceAmt = _ReceAmt;
	}


	/**
	 * @return the _SB
	 */
	public final Integer get_SB() {
		return _SB;
	}


	/**
	 * @param _SB the _SB to set
	 */
	public final void set_SB(Integer _SB) {
		this._SB = _SB;
	}


	/**
	 * @return the _SIA
	 */
	public final Integer get_SIA() {
		return _SIA;
	}


	/**
	 * @param _SIA the _SIA to set
	 */
	public final void set_SIA(Integer _SIA) {
		this._SIA = _SIA;
	}


	/**
	 * @return the _ProjectCode
	 */
	public final String get_ProjectCode() {
		return _ProjectCode;
	}



	/**
	 * @return the _OrgNo
	 */
	public final Long get_OrgNo() {
		return _OrgNo;
	}



	/**
	 * @return the _OrgMemNo
	 */
	public final Long get_OrgMemNo() {
		return _OrgMemNo;
	}



	/**
	 * @return the _MemberName
	 */
	public final String get_MemberName() {
		return _MemberName;
	}



	/**
	 * @return the _TargetAmtSav
	 */
	public final Integer get_TargetAmtSav() {
		return _TargetAmtSav;
	}



	/**
	 * @return the _ColcDate
	 */
	public final Date get_ColcDate() {
		return _ColcDate;
	}



	/**
	 * @return the _SavBalan
	 */
	public final Integer get_SavBalan() {
		return _SavBalan;
	}



	/**
	 * @return the _SavPayable
	 */
	public final Integer get_SavPayable() {
		return _SavPayable;
	}



	/**
	 * @return the _AdjAmt
	 */
	public final Integer get_AdjAmt() {
		return _AdjAmt;
	}



	/**
	 * @return the _CalcIntrAmt
	 */
	public final double get_CalcIntrAmt() {
		return _CalcIntrAmt;
	}

	public final void Save(Integer amt) {
		_ReceAmt += amt;
		_SB += amt;
		
		if((_SIA - amt) >= 0  )
			_SIA -= amt;
		else
			_SIA = 0;
	}

	@Override
	public String toString() {
		return get_OrgNo() + "-"
				+ get_OrgMemNo() + " " + get_MemberName();
	}

}
