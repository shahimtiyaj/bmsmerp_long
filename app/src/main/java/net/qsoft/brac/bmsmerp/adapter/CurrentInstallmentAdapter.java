package net.qsoft.brac.bmsmerp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CIMBModel;

import java.util.ArrayList;

public class CurrentInstallmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<CIMBModel> CInstallmentMissedBorrowerList;
    private ArrayList<CIMBModel> CInstallmentMissedBorrowerListFiltered;
    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int TYPE_TOTAL_AMOUNT = 3;
    private static final int TYPE_FOOTER = 3;
    public static int targetAmount;
    private SwipeRefreshLayout swiper;

    public CurrentInstallmentAdapter(Context context, ArrayList<CIMBModel> CInstallmentMissedBorrowerList) {
        this.context = context;
        setData(CInstallmentMissedBorrowerList);
    }

    public void setData(ArrayList<CIMBModel> CInstallmentMissedBorrowerList) {
        this.CInstallmentMissedBorrowerList = CInstallmentMissedBorrowerList;
        this.CInstallmentMissedBorrowerListFiltered = CInstallmentMissedBorrowerList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_current_installment, parent, false);
            return new MessageViewHolder(itemView);
        } else if (viewType == TYPE_TOTAL_AMOUNT) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.total_amount, parent, false);
            return new TotalAmountViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MessageViewHolder) {
            MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);
        } else if (holder instanceof TotalAmountViewHolder) {
            TotalAmountViewHolder totalAmountViewHolder = (TotalAmountViewHolder) holder;
            totalAmountViewHolder.amountText.setText(String.valueOf("0000"));
        }

    }

    @SuppressLint("SetTextI18n")
    private void setMessageView(final MessageViewHolder viewHolder, final int position) {
        final CIMBModel row = (CIMBModel) CInstallmentMissedBorrowerListFiltered.get(position);
        targetAmount = 0;
        for (int i = 0; i < CInstallmentMissedBorrowerList.size(); i++) {
            targetAmount = targetAmount + CInstallmentMissedBorrowerList.get(i).getTargetAmount();
        }
        if(CInstallmentMissedBorrowerListFiltered.size() == (position + 1)) {
            // Total row
            viewHolder.si_no.setText("");
            viewHolder.st_name.setText("");
            viewHolder.vo_code.setText("");
            viewHolder.member_code.setText("");
            viewHolder.member_name.setText("");
            viewHolder.loan_number.setText("");
            viewHolder.target_date.setText("Total:");
            viewHolder.target_date.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
            viewHolder.target_ammount.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.itemView.setBackgroundResource(R.drawable.footer_border);
            viewHolder.mobile_no.setText("");
        }
        else {
            viewHolder.si_no.setText("" + (position + 1));
            viewHolder.st_name.setText(row.getTerritoryName());
            viewHolder.vo_code.setText(row.getOrgNo());
            viewHolder.member_code.setText(row.getOrgMemNo());
            viewHolder.member_name.setText(row.getMemberName());
            viewHolder.loan_number.setText(row.getLoanNo());
            viewHolder.target_date.setText(row.getTargetDate("dd-MMM-yyyy"));
            viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
            viewHolder.mobile_no.setText(row.getPhoneNo());
//        viewHolder.expDate.setText(String.valueOf("NONE"));
        }
    }


    @Override
    public int getItemCount() {
        return CInstallmentMissedBorrowerListFiltered.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == CInstallmentMissedBorrowerListFiltered.size() + 1) {
            return TYPE_TOTAL_AMOUNT;
        } else {
            return MESSAGE_ITEM_VIEW_TYPE;
        }
    }

    private boolean isPositionAmount(int position) {
        return position == CInstallmentMissedBorrowerListFiltered.size()+1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView si_no, st_name, vo_code, member_code, member_name, loan_number,
                target_date, target_ammount, mobile_no, expDate;

        public MessageViewHolder(final View itemView) {
            super(itemView);

            si_no = itemView.findViewById(R.id.textInstallmentSIno);
            st_name = itemView.findViewById(R.id.textInstallmentSTName);
            vo_code = itemView.findViewById(R.id.CInsMissBorrtextVOCode);
            member_code = itemView.findViewById(R.id.CInsMissBorrtextMemberCode);
            member_name = itemView.findViewById(R.id.CInsMissBorrtextMemberName);
            loan_number = itemView.findViewById(R.id.CInsMissBorrtextLoanNumber);
            target_date = itemView.findViewById(R.id.CInsMissBorrtextTargetdate);
            target_ammount = itemView.findViewById(R.id.CInsMissBorrtextTargetAmount);
            mobile_no = itemView.findViewById(R.id.CInsMissBorrtextMobileNo);
//            expDate = itemView.findViewById(R.id.CInsMissBorrtextExpDateOfColl);
        }

    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    CInstallmentMissedBorrowerListFiltered = CInstallmentMissedBorrowerList;
                } else {
                    ArrayList<CIMBModel> filteredList = new ArrayList<>();
                    for (CIMBModel row : CInstallmentMissedBorrowerList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getMemberName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    CInstallmentMissedBorrowerListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = CInstallmentMissedBorrowerListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                CInstallmentMissedBorrowerListFiltered = (ArrayList<CIMBModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class TotalAmountViewHolder extends RecyclerView.ViewHolder {

        TextView amountText;

        public TotalAmountViewHolder(View view) {
            super(view);
            amountText = (TextView) view.findViewById(R.id.totalSum);
        }
    }
}
