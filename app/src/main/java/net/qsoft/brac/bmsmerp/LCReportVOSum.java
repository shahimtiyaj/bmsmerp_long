package net.qsoft.brac.bmsmerp;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LCReportVOSum extends SSActivity {
    private static final String TAG = LCReportVOSum.class.getSimpleName();

    @BindView(R.id.textRPTHead) TextView mRPTHead;
    @BindView(R.id.okButton) Button cmdOK;
    @BindView(R.id.listViewLCVCS) ListView lv;

    ArrayList<HashMap<String, String>> itms = null;
    Long vono=null;
    Date dtStart=P8.ToDay();
    Date dtEnd = P8.ToDay();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lcreport_vosum);

        ButterKnife.bind(this);

        cmdOK.setVisibility(View.GONE);

        if (getIntent().hasExtra(P8.VONO)) {
            vono = getIntent().getExtras().getLong(P8.VONO,0);
        }
        if(getIntent().hasExtra(LCReportBranchSum.START_DATE)) {
            dtStart = P8.ConvertStringToDate(getIntent().getExtras().getString(LCReportBranchSum.START_DATE), "yyyy-MM-dd");
        }
        if(getIntent().hasExtra(LCReportBranchSum.END_DATE)) {
            dtEnd = P8.ConvertStringToDate(getIntent().getExtras().getString(LCReportBranchSum.END_DATE), "yyyy-MM-dd");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowReport();
    }

    protected void ShowReport() {
        mRPTHead.setText("Loan Repayment Collection\nVO-wise Summary\nVO No.: " + vono);

        DAO da = new DAO(this);
        da.open();
        itms = da.getLoanCollectionVOSummary(vono, dtStart, dtEnd);
        da.close();

        String[] from = new String[] {DBHelper.FLD_ORG_MEM_NO, DBHelper.FLD_COLC_DATE, DBHelper.FLD_TARGET_LOAN_AMT,
                DBHelper.FLD_RECEIVED_AMT ,DBHelper.FLD_LOAN_BAL};
        int[] to = {R.id.textOrgMemNo, R.id.textColcDate, R.id.textTargetdAmt, R.id.textCollectedAmt, R.id.textLoanOutstanding };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), itms, R.layout.lcreport_vo_sum_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mVONo = (TextView) v.findViewById(R.id.textOrgMemNo);
                String x = mVONo.getText().toString();
                if(x.equals("Total") && (position == itms.size() - 1)) {
                    v.setBackgroundResource(R.color.black_overlay);
                }
                return v;
            }
        };

        lv.setAdapter(adapter);
    }

    public void onCancel(View view) {
        // Back
        finish();
    }

}
