package net.qsoft.brac.bmsmerp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.Followup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowupActivity extends SSActivity {

    @BindView(R.id.textODL_L) TextView textODL_L;
    @BindView(R.id.textODL_V) TextView textODL_V;
    @BindView(R.id.textNewAd_L) TextView textNewAd_L;
    @BindView(R.id.textNewAd_V) TextView textNewAd_V;
    @BindView(R.id.textNewSCM_L) TextView textNewSCM_L;
    @BindView(R.id.textNewSCM_V) TextView textNewSCM_V;
    @BindView(R.id.textOldSCM_L) TextView textOldSCM_L;
    @BindView(R.id.textOldSCM_V) TextView textOldSCM_V;
    @BindView(R.id.textSBW_L) TextView textSBW_L;
    @BindView(R.id.textSBW_V) TextView textSBW_V;

    @BindView((R.id.cancelButton)) Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_followup);
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        cancelButton.setVisibility(View.GONE);

        DAO da = new DAO(this);
        da.open();
        ArrayList<Followup> fuAll = da.getFollowupData(P8.ToDay());
        da.close();

        for(Followup fu:fuAll ) {
            if(fu.get_FCType() == 1)
                textODL_V.setText(fu.get_Comment());
            else if(fu.get_FCType() == 2)
                textNewAd_V.setText(fu.get_Comment());
            else if(fu.get_FCType() == 3)
                textNewSCM_V.setText(fu.get_Comment());
            else if(fu.get_FCType() == 4)
                textOldSCM_V.setText(fu.get_Comment());
            else if(fu.get_FCType() == 5)
                textSBW_V.setText(fu.get_Comment());
        }
    }

    public void onComments1(View v) {
        launchComments(1, textODL_L.getText().toString(), textODL_V.getText().toString());
    }

    public void onComments2(View v) {
        launchComments(2, textNewAd_L.getText().toString(), textNewAd_V.getText().toString());
    }

    public void onComments3(View v) {
        launchComments(3, textNewSCM_L.getText().toString(), textNewSCM_V.getText().toString());
    }
    public void onComments4(View v) {
        launchComments(4, textOldSCM_L.getText().toString(), textOldSCM_V.getText().toString());
    }
    public void onComments5(View v) {
        launchComments(5, textSBW_L.getText().toString(), textSBW_V.getText().toString());
    }

    private void launchComments(int reqid, String txtHdr, String txtCmnts) {
        Intent it =  new Intent(this, CommentsActivity.class);
        it.putExtra(CommentsActivity.CALLERNAME, getTitle().toString());
        it.putExtra(CommentsActivity.COMMENTSLABEL, txtHdr);
        it.putExtra(CommentsActivity.COMMENTS, txtCmnts);
        startActivityForResult(it, reqid);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String x;

        if(resultCode == Activity.RESULT_OK) {
            x = data.getStringExtra(CommentsActivity.COMMENTS).toString();
            if (requestCode == 1)
                textODL_V.setText(x);
            else if (requestCode == 2)
                textNewAd_V.setText(x);
            else if (requestCode == 3)
                textOldSCM_V.setText(x);
            else if (requestCode == 4)
                textOldSCM_V.setText(x);
            else if (requestCode == 5)
                textSBW_V.setText(x);

            SaveData(requestCode, x);
        }
    }

    private void SaveData(int fc, String cmnts) {
        DAO da = new DAO(this);
        da.open();
        da.updateFollowupData(P8.ToDay(), fc, cmnts);
        da.close();
    }

    public void onOk(View v) {
        // Savings/Housekeeping
        // and back to caller
        finish();
    }

    public void onCancel(View v) {
        // Back to caller
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onOk(null);
    }
}
