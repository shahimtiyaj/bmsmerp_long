package net.qsoft.brac.bmsmerp.data;

import net.qsoft.brac.bmsmerp.P8;

import java.util.Date;

public class CIMBModel {
    private String TerritoryName;
    private String OrgNo;
    private String OrgMemNo;
    private String MemberName;
    private String LoanNo;
    private String TargetDate;
    private int TargetAmount;
    private String PhoneNo;
    private String ExpColcDate;

    public CIMBModel(){

    }

    public CIMBModel(String territoryName, String orgNo, String orgMemNo, String memberName,
                     String loanNo, String targetDate , int targetAmount, String phoneNo){

    }

    public String getTerritoryName() {
        return TerritoryName;
    }

    public void setTerritoryName(String territoryName) {
        TerritoryName = territoryName;
    }

    public String getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(String orgNo) {
        OrgNo = orgNo;
    }

    public String getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(String orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public String getTargetDate() {
        return TargetDate;
    }
    public String getTargetDate(String frmt) {
        Date dt = P8.ConvertStringToDate(TargetDate);
        return P8.FormatDate(dt, frmt);
    }

    public void setTargetDate(String targetDate) {
        TargetDate = targetDate;
    }

    public int getTargetAmount() {
        return TargetAmount;
    }

    public void setTargetAmount(int targetAmount) {
        TargetAmount = targetAmount;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getExpColcDate() {
        return ExpColcDate;
    }

    public void setExpColcDate(String expColcDate) {
        ExpColcDate = expColcDate;
    }

}
