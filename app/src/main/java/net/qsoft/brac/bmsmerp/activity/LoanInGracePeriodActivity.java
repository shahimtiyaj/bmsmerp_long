package net.qsoft.brac.bmsmerp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.qsoft.brac.bmsmerp.App;
import net.qsoft.brac.bmsmerp.P8;
import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.adapter.LoanInGracePeriodAdapter;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.LGPModel;
import net.qsoft.brac.bmsmerp.data.PO;

import java.util.ArrayList;

public class LoanInGracePeriodActivity extends Activity {
    private static final String TAG = LoanInGracePeriodActivity.class.getSimpleName();

    private LinearLayoutManager layoutManager;
    private LoanInGracePeriodAdapter adapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private ArrayList<LGPModel> loanGraceArrayList;
    private RelativeLayout mainLayout, emptyLayout;
    private Button cmdOK;
    private TextView branchName, CInsMissBorrtotalTargetSum, voName;

    private String pono = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_in_grace_period_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        cmdOK = (Button) findViewById(R.id.okButton);
        ((Button) findViewById(R.id.cancelButton)).setVisibility(View.GONE);
        branchName = (TextView) findViewById(R.id.textBranchName);

        if(getIntent().hasExtra(P8.PONO)) {
            pono = getIntent().getExtras().getString(P8.PONO);
        }

        loanGraceArrayList = new ArrayList<>();
        adapter = new LoanInGracePeriodAdapter(this, loanGraceArrayList);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.loan_grace_list_recycler_view);
        // swipeRefresh = findViewById(R.id.swipe_refresh_layout);
        mainLayout = findViewById(R.id.main_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        loadCurrentSchedule();
    }

    private void loadCurrentSchedule() {
        DAO dao = new DAO(App.getContext());
        dao.open();
        PO po = dao.getPO();
        loanGraceArrayList = dao.getLoanInGracePeriod(pono);
        CO co = dao.getCO(pono);
        dao.close();

        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName()
                + "\n" + co.get_CONo() + " - " + co.get_COName());

        adapter = new LoanInGracePeriodAdapter(App.getContext(), loanGraceArrayList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
            // swipeRefresh.setRefreshing(false);
    }

    public void onOk(View view) {
        finish();
    }

}
