package net.qsoft.brac.bmsmerp.data;

import net.qsoft.brac.bmsmerp.P8;

import java.util.Date;

public class LGPModel {

    private String TerritoryName;
    private String OrgNo;
    private String OrgMemNo;
    private String MemberName;
    private String LoanNo;
    private Date DisbDate;
    private int TargetAmount;
    private Date CollDateAfterDisb;
    private Date ActualTargetDate;

    public LGPModel() {

    }

    public LGPModel(String territoryName, String orgNo, String orgMemNo, String memberName,
                    String loanNo, String DisbDate, int targetAmount,
                    int collDateAfterDisb, String phoneNo) {
    }

    public String getTerritoryName() {
        return TerritoryName;
    }

    public void setTerritoryName(String territoryName) {
        TerritoryName = territoryName;
    }

    public String getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(String orgNo) {
        OrgNo = orgNo;
    }

    public String getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(String orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public Date getDisbDate() {
        return DisbDate;
    }
    public String getDisbDate(String fmt) {
        return P8.FormatDate(DisbDate, fmt);
    }
    public void setDisbDate(String disbDate) {
        DisbDate = P8.ConvertStringToDate(disbDate);
    }

    public Integer getTargetAmount() {
        return TargetAmount;
    }

    public void setTargetAmount(int targetAmount) {
        TargetAmount = targetAmount;
    }

    public Date getCollDateAfterDisb() {
        return CollDateAfterDisb;
    }

    public String getCollDateAfterDisb(String fmt) {
        return P8.FormatDate(CollDateAfterDisb, fmt);
    }

    public void setCollDateAfterDisb(Date collDateAfterDisb) {
        CollDateAfterDisb = collDateAfterDisb;
    }

    public Date getActualTargetDate() {
        return ActualTargetDate;
    }
    public String getActualTargetDate(String fmt) {
        return P8.FormatDate(ActualTargetDate, fmt);
    }

    public void setActualTargetDate(String actualTargetDate) {
        ActualTargetDate = P8.ConvertStringToDate(actualTargetDate);
    }


}
