package net.qsoft.brac.bmsmerp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.DAO;

import java.util.ArrayList;
import java.util.List;

public class COListActivity extends SSActivity {
    private static final String TAG = COListActivity.class.getSimpleName();

    ListView lv = null;
    ArrayList<String> items=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_colist);
        super.onCreate(savedInstanceState);

        lv = (ListView) findViewById(R.id.lstView);

        DAO da = new DAO(this);
        da.open();
        final List<CO> cos = da.getCOList();
        da.close();

        for(CO co:cos)
            items.add(co.get_CONo() + "      " + co.get_COName());

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, items); /*{
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(getResources().getColor(R.color.list_row_text));
                return view;
            }
        };*/

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String x = lv.getItemAtPosition(position).toString();
                x = x.substring(0, x.indexOf(" ")).trim();
                startAction(2, x);
            //    Toast.makeText(getBaseContext(), x, Toast.LENGTH_LONG).show();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String x = cos.get(position).get_CONo();
                startAction(1, x);
            //    Toast.makeText(getBaseContext(), "Long pressed: " + x , Toast.LENGTH_LONG).show();

                return true;
            }
        });

        lv.setAdapter(adapter);
    }

    protected void startAction(Integer opt, String pono) {
        if(opt==1 || opt == 2) {
            // PO Dashboard
            Intent it = new Intent(this, TTActivity.class);
            it.putExtra(P8.PONO, pono);
//			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);
//			finish();
        }
//        else if(opt==2) {
//            // VO List
//            Intent it = new Intent(this, VOActivity.class);
//            it.putExtra(P8.PONO, pono);
//            startActivity(it);
//        }
    }


    public void onCancel(View view) {
        // Back
        finish();
    }
}
