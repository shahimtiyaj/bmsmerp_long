package net.qsoft.brac.bmsmerp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import net.qsoft.brac.bmsm.R;


import net.qsoft.brac.bmsmerp.P8;
import net.qsoft.brac.bmsmerp.data.CSMBModel;

import java.util.ArrayList;

public class CurrentScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<CSMBModel> stringArrayList;
    private Context context;

    public CurrentScheduleAdapter(Context context, ArrayList<CSMBModel> strings) {
        this.context = context;
        setData(strings);
    }

    public void setData(ArrayList<CSMBModel> strings) {
        this.stringArrayList = strings;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_current_schedule, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_HEADER) {
            //Inflating header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.total_amount, parent, false);
            return new HeaderViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_footer_layout, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            // headerHolder.headerTitle.setText("Header View");

        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.footerText.setText("Footer View");

        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder viewHolder = (ItemViewHolder) holder;

            final CSMBModel row = (CSMBModel) stringArrayList.get(position);

            if(stringArrayList.size() == (position + 1)) {
                viewHolder.si_no.setText("");
                viewHolder.st_name.setText("");
                viewHolder.vo_code.setText("");
                viewHolder.member_code.setText("");
                viewHolder.member_name.setText("");
                viewHolder.loan_number.setText("");
                viewHolder.target_date.setText("");
                viewHolder.coll_date.setText("Total: ");
                viewHolder.coll_date.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                viewHolder.target_ammount.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.coll_amount.setText(String.format("%,d", row.getCollAmount()));
                viewHolder.coll_amount.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.mobile_no.setText("");
                viewHolder.itemView.setBackgroundResource(R.drawable.footer_border);
            }
            else {
                viewHolder.si_no.setText("" + (position + 1));
                viewHolder.st_name.setText(row.getTerritoryName());
                viewHolder.vo_code.setText(row.getOrgNo().toString());
                viewHolder.member_code.setText(row.getOrgMemNo().toString());
                viewHolder.member_name.setText(row.getMemberName());
                viewHolder.loan_number.setText(row.getLoanNo());
                viewHolder.target_date.setText((row.getTargetDate() == null) ? "" : P8.FormatDate(row.getTargetDate(), "dd-MMM-yyyy"));
                viewHolder.coll_date.setText((row.getCollDate() == null) ? "" : P8.FormatDate(row.getCollDate(), "dd-MMM-yyyy"));
                viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                viewHolder.coll_amount.setText(String.format("%,d", row.getCollAmount()));
                viewHolder.mobile_no.setText(row.getPhoneNo());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == 0) {
//            return TYPE_HEADER;
//        }
//        else

            if (position == stringArrayList.size()+1) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;

        }
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size() ;
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTitle;

        public HeaderViewHolder(View view) {
            super(view);
            headerTitle = (TextView) view.findViewById(R.id.totalSum);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView si_no, st_name, vo_code, member_code, member_name, loan_number,
                target_date, coll_date, target_ammount, coll_amount, mobile_no;

        public ItemViewHolder(View itemView) {
            super(itemView);

                si_no = itemView.findViewById(R.id.textSIno);
                st_name = itemView.findViewById(R.id.textSTName);
                vo_code = itemView.findViewById(R.id.textVOCode);
                member_code = itemView.findViewById(R.id.textMemberCode);
                member_name = itemView.findViewById(R.id.textMemberName);
                loan_number = itemView.findViewById(R.id.textLoanNumber);
                target_date = itemView.findViewById(R.id.textTargetdate);
                coll_date = itemView.findViewById(R.id.textColldate);
                target_ammount = itemView.findViewById(R.id.textTargetAmount);
                coll_amount = itemView.findViewById(R.id.textCollAmount);
                mobile_no = itemView.findViewById(R.id.textMobileNo);
        }
    }
}
