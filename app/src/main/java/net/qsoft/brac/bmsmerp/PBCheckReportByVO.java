package net.qsoft.brac.bmsmerp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.VO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class PBCheckReportByVO extends SSActivity {
    private static final String TAG = PBCheckReportByVO.class.getSimpleName();
    public static final String PBCR_REPORT_TYPE="PBCR_REPORT_TYPE";

    TextView mRPTType;
    TextView mCumBalHT;
    TextView mVOName;
    TextView mCOName;

    ListView lv;
    TextView mComments;
    TextView mStats;
    TextView mDateFrom;
    TextView mDateTo;

    String sRPTType = "Savings";
    Long vono;
    Date datStart;
    Date datEnd;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    ArrayList<HashMap<String, String>> itms = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbcheck_report_by_vo);

        Button okButton = (Button) findViewById(R.id.okButton);
        okButton.setVisibility(View.GONE);
        mRPTType = (TextView) findViewById(R.id.textRPTType);
        mCumBalHT = (TextView) findViewById(R.id.textCumBalHT);
        mVOName = (TextView) findViewById(R.id.textVOName);
        mCOName = (TextView) findViewById(R.id.textCO);
        mDateFrom = (TextView) findViewById(R.id.textDateFrom);
        mDateTo = (TextView) findViewById(R.id.textDateTo);

        lv = (ListView) findViewById(R.id.listViewPBCR);
//        mComments = (TextView) findViewById(R.id.textComments);
        mStats = (TextView) findViewById(R.id.textStats);

        Intent it = getIntent();
        if (it.hasExtra(P8.VONO))
            vono = it.getLongExtra(P8.VONO,0);

//        if(it.hasExtra(PBCR_REPORT_TYPE))
//            sRPTType = it.getStringExtra(PBCR_REPORT_TYPE);


        datEnd = P8.ToDay();
        Calendar newCal = Calendar.getInstance();
        newCal.setTime(datEnd);
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datEnd = c.getTime();
//                ShowDateLabels();
                ShowReport();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));

        newCal.set(Calendar.DAY_OF_MONTH, 1);
        datStart = newCal.getTime();

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datStart = c.getTime();
//                ShowDateLabels();
                ShowReport();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowReport();
    }

    private void ShowDateLabels() {
            mDateFrom.setText("From date: " + P8.FormatDate(datStart, "dd-MMM-yyyy"));
            mDateTo.setText("To: " + P8.FormatDate(datEnd, "dd-MMM-yyyy"));
    }

    private void ShowReport() {
        mRPTType.setText("(" + sRPTType + ")");
        if (sRPTType.equals("Savings"))
            mCumBalHT.setText("Cumulative Savings Balance");
        else
            mCumBalHT.setText("Cumulative Loan Realized");

        ShowDateLabels();

        DAO da = new DAO(this);
        da.open();
        VO vo = da.getVO(vono);
        String cono = vo.get_CONo();
        mVOName.setText("VO: " + vo.toString() );  //vono + " - " + vo.get_OrgName());
        mCOName.setText("CO: " + cono.trim() + " - " + vo.get_COName());

        itms = da.getPBCheckReportByVO(sRPTType, vono, datStart, datEnd);
        da.close();

        String[] from = new String[] {"[OrgMemNo]", "[VisitDate]", "[BALSYS]", "[BALPSS]", "[DIFF]"};
        int[] to = {R.id.textMemNo, R.id.textColcDate, R.id.textCumBalSys, R.id.textCumBalPas, R.id.textDifference};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), itms, R.layout.pbcheck_by_vo_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mOrgMemNo = (TextView) v.findViewById(R.id.textMemNo);
                String x = mOrgMemNo.getText().toString();
//                HashMap<String, String> row = (HashMap<String, String>) getItem(position);
                //if (row.get("[OrgMemNo]").equals("Total") || row.get("[OrgMemNo]").equals("Count")) {
                if(x.equals("Total") || x.equals("Count")) {
                    //v.setBackgroundResource(R.drawable.cust_border);
                   v.setBackgroundResource(R.color.black_overlay);
                }

                return v;
            }
        };
        lv.setAdapter(adapter);
    }

    public void onDateClick(View v) {
        if(v.getId() == R.id.textDateFrom)
            fromDatePickerDialog.show();
        else if(v.getId() == R.id.textDateTo)
            toDatePickerDialog.show();
    }

    public void onHeaderClick(View v) {
        if(sRPTType.equals("Savings"))
            sRPTType = "Loan";
        else
            sRPTType = "Savings";
        ShowReport();
    }

    public void onVOClick(View v) {
        startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 206);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 206: {
                if (resultCode == RESULT_OK) {
                    vono = data.getLongExtra(P8.VONO,0);
                    ShowReport();
                }
            }
            break;
        }
    }

    public void onCancel(View v) {
        finish();
    }
}
