package net.qsoft.brac.bmsmerp.data;


import net.qsoft.brac.bmsmerp.P8;

import java.util.Date;

public class CDGPModel {

    private String TerritoryName;
    private String OrgNo;
    private String OrgMemNo;
    private String MemberName;
    private String LoanNo;
    private Date TargetDate;
    private int TargetAmount;
    private int CollAmount;
    private String PhoneNo;


    public String getTerritoryName() {
        return TerritoryName;
    }

    public void setTerritoryName(String territoryName) {
        TerritoryName = territoryName;
    }

    public String getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(String orgNo) {
        OrgNo = orgNo;
    }

    public String getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(String orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public Date getTargetDate() {
        return TargetDate;
    }
    public String getTargetDate(String fmt) {
        return P8.FormatDate(TargetDate, fmt);
    }

    public void setTargetDate(String targetDate) {
        TargetDate = P8.ConvertStringToDate(targetDate);
    }

    public void setTargetDate(Date targetDate) {
        TargetDate = targetDate;
    }

    public int getTargetAmount() {
        return TargetAmount;
    }

    public void setTargetAmount(int targetAmount) {
        TargetAmount = targetAmount;
    }

    public int getCollAmount() {
        return CollAmount;
    }

    public void setCollAmount(int collAmount) {
        CollAmount = collAmount;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}
