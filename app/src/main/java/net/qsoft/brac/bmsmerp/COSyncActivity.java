package net.qsoft.brac.bmsmerp;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class COSyncActivity extends SSActivity {

    @BindView(R.id.lstView)
    ListView lv = null;
    ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_cosync);
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        DAO da = new DAO(this);
        da.open();
        final List<CO> cos = da.getCOList();
        da.close();

        HashMap<String, String> map;

        for (CO co : cos) {

            map = new HashMap<String, String>();
            map.put(DBHelper.FLD_CONO, co.get_CONo());
            map.put(DBHelper.FLD_CONAME, co.get_COName());
           // map.put(DBHelper.FLD_COLastyncTime, String.valueOf(co.get_LastSyncTime()));
           map.put(DBHelper.FLD_COLastyncTime, P8.FormatDate(co.get_LastSyncTime(), "dd-MMM-yyyy hh:mm:ss"));// previous
            // map.put(DBHelper.FLD_COLastyncTime, P8.FormatDate(co.get_LastSyncTime(), "EEE MMM dd HH:mm:ss zzz yyyy"));

            items.add(map);
        }

        String[] from = new String[]{DBHelper.FLD_CONO, DBHelper.FLD_CONAME, DBHelper.FLD_COLastyncTime};
        int[] to = {R.id.textCONo, R.id.textCOName, R.id.texSyncTime};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.cosync_row, from, to);

        lv.setAdapter(adapter);

    }

    public void onCancel(View view) {
        // Back
        finish();
    }
}
