package net.qsoft.brac.bmsmerp.data;

import java.util.Date;

/**
 * Created by zferdaus on 11/2/2016.
 */

public class CO {
    private String _CONo;
    private String _COName;
    private Date _LastSyncTime;

    public CO(String _CONo, String _COName) {
        this._CONo = _CONo;
        this._COName = _COName;
    }

    public CO(String _CONo, String _COName, Date _LastSyncTime) {
        this._CONo = _CONo;
        this._COName = _COName;
        this._LastSyncTime = _LastSyncTime;
    }

    public String get_CONo() {
        return _CONo;
    }
    public String get_COName() {
        return _COName;
    }
    public Date get_LastSyncTime() {
        return _LastSyncTime;
    }
}
