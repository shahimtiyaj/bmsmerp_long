package net.qsoft.brac.bmsmerp.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.qsoft.brac.bmsmerp.App;
import net.qsoft.brac.bmsmerp.P8;
import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.adapter.CurrentScheduleAdapter;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.CSMBModel;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.PO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CurrentScheduleActivity extends Activity {
    private static final String TAG = CurrentScheduleActivity.class.getSimpleName();

    private LinearLayoutManager layoutManager;
    private CurrentScheduleAdapter adapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private ArrayList<CSMBModel> messagesArrayList;
    private RelativeLayout mainLayout, emptyLayout;
    private Button cmdOK;
    private TextView branchName, CInsMissBorrtotalTargetSum, voName;
    private TextView mDateFrom;
    private TextView mDateTo;

    private Date datStart, datEnd;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private String pono = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_schedul_mb_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        cmdOK = (Button) findViewById(R.id.okButton);
        ((Button) findViewById(R.id.cancelButton)).setVisibility(View.GONE);

        branchName = (TextView) findViewById(R.id.textBranchName);
        mDateFrom = (TextView) findViewById(R.id.textDateFrom);
        mDateTo = (TextView) findViewById(R.id.textDateTo);

        if(getIntent().hasExtra(P8.PONO)) {
            pono = getIntent().getExtras().getString(P8.PONO);
        }

        messagesArrayList = new ArrayList<>();
        adapter = new CurrentScheduleAdapter(this, messagesArrayList);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.cs_list_recycler_view);
        mainLayout = findViewById(R.id.main_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);

        mDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateClick(v);
            }
        });
        mDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateClick(v);
            }
        });
        datEnd = P8.ToDay();
        Calendar newCal = Calendar.getInstance();
        newCal.setTime(datEnd);
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datEnd = c.getTime();
                loadCurrentSchedule();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));

        newCal.set(Calendar.DAY_OF_MONTH, 1);
        datStart = newCal.getTime();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datStart = c.getTime();
                loadCurrentSchedule();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadCurrentSchedule();
    }

    public void onDateClick(View v) {
        if(v.getId() == R.id.textDateFrom)
            fromDatePickerDialog.show();
        else if(v.getId() == R.id.textDateTo)
            toDatePickerDialog.show();
    }

    private void ShowDateLabels() {
        mDateFrom.setText("From date: " + P8.FormatDate(datStart, "dd-MMM-yyyy"));
        mDateTo.setText("To: " + P8.FormatDate(datEnd, "dd-MMM-yyyy"));
    }

    private void loadCurrentSchedule() {
        ShowDateLabels();
        DAO dao = new DAO(App.getContext());
        dao.open();
        PO po = dao.getPO();
        messagesArrayList = dao.getCurrentScheduleData(pono, datStart, datEnd);
        CO co = dao.getCO(pono);
        dao.close();

        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName()
                + "\n" + co.get_CONo() + " - " + co.get_COName());

        adapter.setData(messagesArrayList);
    }

    public void onOk(View view) {
        finish();
    }
}
