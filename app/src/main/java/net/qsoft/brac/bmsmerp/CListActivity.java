package net.qsoft.brac.bmsmerp;

import java.util.ArrayList;
import java.util.HashMap;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class CListActivity extends SSActivity {
	private static final String TAG = CListActivity.class.getSimpleName();
	
	ListView lv = null;
	ArrayList<HashMap<String, String>> items=null;
	Long vono=null;
	String pono=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_clist);
		super.onCreate(savedInstanceState);

		lv = (ListView) findViewById(R.id.lstView);
		
		if(getIntent().hasExtra(P8.VONO)) {
			vono = getIntent().getExtras().getLong(P8.VONO);
//			Log.d(TAG, "In has extra " + vono);
			setTitle("Collec. Details VO No.: " + vono);
		}
		else if(getIntent().hasExtra(P8.PONO)) {
			pono = getIntent().getExtras().getString(P8.PONO);
			setTitle("Collec. Sumamry CO/PO No.: " + pono);
		} else {
			setTitle("Collection Details");
		}
		getSupportActionBar().setTitle(getTitle());

		createCollectionList(vono, pono);
	}
	
	private void createCollectionList(Long von, String pon) {
//		Log.d(TAG, "VO No.: " + (von==null ? "": von));

		DAO da = new DAO(this);
		da.open();
		items = da.getCollectionItems(von, pon);
		da.close();
		
		CListAdapter adapter = new CListAdapter(this, R.layout.cl_gp_detail, items); 
		lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
	
}
