package net.qsoft.brac.bmsmerp.data;

import java.util.Date;

/**
 * Created by zferdaus on 2/18/2017.
 */

public class VOVisit {
    Date _VisitDate;
    Long _OrgNo;
    Integer _TotalMemb;
    Integer _PrsentMemb;
    Integer _TotalBorrower;
    Integer _CurrentBorrower;
    String _PBComment;
    Date _LastUpdate;

    public VOVisit(Date _VisitDate, Long _OrgNo, Integer _TotalMemb, Integer _PrsentMemb, Integer _TotalBorrower, Integer _CurrentBorrower, String _PBComment, Date _LastUpdate) {
        this._VisitDate = _VisitDate;
        this._OrgNo = _OrgNo;
        this._TotalMemb = _TotalMemb;
        this._PrsentMemb = _PrsentMemb;
        this._TotalBorrower = _TotalBorrower;
        this._CurrentBorrower = _CurrentBorrower;
        this._PBComment = _PBComment;
        this._LastUpdate = _LastUpdate;
    }

    public VOVisit() {
//        _VisitDate.setTime(0);
    }

    public Date get_VisitDate() {
        return _VisitDate;
    }

    public void set_VisitDate(Date _VisitDate) {
        this._VisitDate = _VisitDate;
    }

    public Long get_OrgNo() {
        return _OrgNo;
    }

    public void set_OrgNo(Long _OrgNo) {
        this._OrgNo = _OrgNo;
    }

    public Integer get_TotalMemb() {
        return _TotalMemb;
    }

    public void set_TotalMemb(Integer _TotalMemb) {
        this._TotalMemb = _TotalMemb;
    }

    public Integer get_PrsentMemb() {
        return _PrsentMemb;
    }

    public void set_PrsentMemb(Integer _PrsentMemb) {
        this._PrsentMemb = _PrsentMemb;
    }

    public Integer get_TotalBorrower() {
        return _TotalBorrower;
    }

    public void set_TotalBorrower(Integer _TotalBorrower) {
        this._TotalBorrower = _TotalBorrower;
    }

    public Integer get_CurrentBorrower() {
        return _CurrentBorrower;
    }

    public void set_CurrentBorrower(Integer _CurrentBorrower) {
        this._CurrentBorrower = _CurrentBorrower;
    }

    public String get_PBComment() {
        return _PBComment;
    }

    public void set_PBComment(String _PBComment) {
        this._PBComment = _PBComment;
    }

    public Date get_LastUpdate() {
        return _LastUpdate;
    }

    public void set_LastUpdate(Date _LastUpdate) {
        this._LastUpdate = _LastUpdate;
    }
}
