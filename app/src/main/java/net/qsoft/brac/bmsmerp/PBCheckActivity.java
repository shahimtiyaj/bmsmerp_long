package net.qsoft.brac.bmsmerp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;
import net.qsoft.brac.bmsmerp.data.VO;
import net.qsoft.brac.bmsmerp.data.VOVisit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PBCheckActivity extends SSActivity {
    private static final String TAG = PBCheckActivity.class.getSimpleName();

    TextView mVOCode;
    TextView mDate;
    TextView mCO;
    TextView mtvTotMem;
    EditText meditTotPresent;
    TextView mtextTotBor;
    TextView mtextTotCur;

    TextView mtextCkSavings;
    TextView mtextCkLoan;
    TextView mtextCkTotal;
    TextView mtextCkSavingsMis;
    TextView mtextCkLoanMis;
    TextView mtextCkTotalMis;

    ListView lv = null;
    ListView lva = null;

    TextView mtextMemNo;
    TextView mtextMemName;

    TableRow mrowBalPrompt;
    TableRow mrowBalEdit;
    TableRow mrowRadio;
    TableRow mrowButton;

    EditText meditPassBall;
    RadioGroup mradioLoc;

    TextView meditPBComments;

    Date datVisitDate;
    Long vono;
    String cono;
    VOVisit vv;

    ArrayList<HashMap<String, String>> memlist = null;
    SimpleAdapter adapter;

    HashMap<String, String> memmap = null;
    HashMap<String, String> vmap = null;

    Button btnSave;
    Button btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbcheck);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(getTitle());
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mVOCode = (TextView) findViewById(R.id.textVOCode);
        mDate = (TextView) findViewById(R.id.textDate);
        mCO = (TextView) findViewById(R.id.textCO);

        mtvTotMem = (TextView) findViewById(R.id.tvTotMem);
        meditTotPresent = (EditText) findViewById(R.id.editTotPresent);
        mtextTotBor = (TextView) findViewById(R.id.textTotBor);
        mtextTotCur = (TextView) findViewById(R.id.textTotCur);

        mtextCkSavings = (TextView) findViewById(R.id.textCkSavings);
        mtextCkLoan = (TextView) findViewById(R.id.textCkLoan);
        mtextCkTotal = (TextView) findViewById(R.id.textCkTotal);
        mtextCkSavingsMis = (TextView) findViewById(R.id.textCkSavingsMis);
        mtextCkLoanMis = (TextView) findViewById(R.id.textCkLoanMis);
        mtextCkTotalMis = (TextView) findViewById(R.id.textCkTotalMis);

        mtextMemNo = (TextView) findViewById(R.id.textMemNo);
        mtextMemName = (TextView) findViewById(R.id.textMemName);

        mrowBalPrompt = (TableRow) findViewById(R.id.rowBalPrompt);
        mrowBalEdit = (TableRow) findViewById(R.id.rowBalEdit);
        mrowRadio = (TableRow) findViewById(R.id.rowRadio);
        mrowButton = (TableRow) findViewById(R.id.rowButton);

        meditPassBall = (EditText) findViewById(R.id.editPassBal);
        mradioLoc = (RadioGroup) findViewById(R.id.radioLoc);

        lv = (ListView) findViewById(R.id.lstView);
        lva = (ListView) findViewById(R.id.lstArticles);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        meditPBComments = (TextView) findViewById(R.id.editPBComments);

        mDate.setText("Date: " + P8.FormatDate(P8.ToDay(), "MMMM dd, yyyy"));

        Intent it = getIntent();
        if (it.hasExtra(P8.VONO))
            vono = it.getLongExtra(P8.VONO,0);

        datVisitDate = P8.ToDay();
     }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
    }

    private void saveData() {
        if(vv != null) {
            String x = meditTotPresent.getText().toString();
            int xv = 0;
            try {
                xv = Integer.parseInt(x);
            }
            catch (Exception e){
            }
            if(vv.get_PrsentMemb() != xv ||
                !vv.get_PBComment().equals(meditPBComments.getText().toString().trim())) {
                vv.set_PrsentMemb(xv);
                vv.set_PBComment(meditPBComments.getText().toString().trim());
                DAO da = new DAO(this);
                da.open();
                try {
                    da.UpdateVOVisit(vv);
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            }
        }
    }

    private void loadData() {
        DAO da = new DAO(this);
        da.open();
        VO vo = da.getVO(vono);
        cono = vo.get_CONo();
        mVOCode.setText("VO: " + vo.toString() );  //vono + " - " + vo.get_OrgName());
        mCO.setText("CO: " + cono.trim() + " - " + vo.get_COName());

        vv = da.getVOVisit(datVisitDate, vono);
        memlist = da.getVOVistMemberList(datVisitDate, vono);
        da.close();

        if (vv != null)
            refreshView();
        else {
            DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        createVOVisit();
                        refreshView();
                    } else {
                        PBCheckActivity.this.finish();
                    }
                }
            };

            P8.ErrorSound(this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Create VO Visit")
                    .setMessage("VO No.: " + vono + "\n\nThis is " +
                            ((vo.isTodaysVO()) ? "" : "not") + " a scheduled VO to visit!!!\n\n" +
                            "Do you want to check passbooks for this VO?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, dialogClick)
                    .setNegativeButton(android.R.string.no, dialogClick).show();
        }
    }

    private void refreshVOBCheckup() {
        DAO da = new DAO(this);
        da.open();
        HashMap<String, Integer[]> stat = da.getVOBCheckStatByVO(datVisitDate, vono);
        da.close();

        Integer[] aa = stat.get("checked");
        mtextCkSavings.setText(String.format("%d", aa[0]));
        mtextCkLoan.setText(String.format("%d", aa[1]));
        mtextCkTotal.setText(String.format("%d", aa[2]));

        aa = stat.get("mismatched");
        mtextCkSavingsMis.setText(String.format("%d", aa[0]));
        mtextCkLoanMis.setText(String.format("%d", aa[1]));
        mtextCkTotalMis.setText(String.format("%d", aa[2]));
    }

    private void createVOVisit() {
        vv = new VOVisit();
        vv.set_VisitDate(datVisitDate);
        vv.set_OrgNo(vono);

        DAO da = new DAO(this);
        da.open();

        vv.set_TotalMemb(da.getMemberCount(vono));
        vv.set_PrsentMemb(0);
        Integer[] borrowers = da.getTotalBorrowsers(vono, null);

        vv.set_TotalBorrower(borrowers[0]);
        vv.set_CurrentBorrower(borrowers[1]);
        vv.set_PBComment("");

        try {
            da.UpdateVOVisit(vv);
        } catch (Exception e) {
            P8.ShowError(this, e.getMessage());
            this.finish();
        } finally {
            da.close();
        }
    }

    private void refreshView() {
//        mtextTotMem.setText(memlist.size());

        mtvTotMem.setText(String.format("%d", vv.get_TotalMemb()));
        meditTotPresent.setText(String.format("%d", vv.get_PrsentMemb()));
        mtextTotBor.setText(String.format("%d", vv.get_TotalBorrower()));
        mtextTotCur.setText(String.format("%d", vv.get_CurrentBorrower()));
        meditPBComments.setText(vv.get_PBComment());
        meditTotPresent.requestFocus();

        refreshVOBCheckup();

        String[] from = new String[]{"[OrgNo]", "[OrgMemNo]", "[MemberName]"};
        int[] to = {R.id.textVONo, R.id.textMemNo, R.id.textMemName};

        adapter = new SimpleAdapter(getApplicationContext(), memlist,
                R.layout.member_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View t = super.getView(position, convertView, parent);
                HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
                if (x.get("HAS_VOVISIT_RECORD").equals("1"))
                    t.setBackgroundResource(R.color.Aquamarine);
                else
                    t.setBackgroundResource(R.color.form_background);
                return t;
            }
        };

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                memmap = (HashMap<String, String>) lv.getItemAtPosition(position);
                //String vono = x.get(DBHelper.FLD_ORG_NO);
                Long memno = Long.valueOf(memmap.get(DBHelper.FLD_ORG_MEM_NO));
                mtextMemNo.setText("Mem No.:  " + memno);
                mtextMemName.setText("Name: " + memmap.get(DBHelper.FLD_ORG_MEM_NAME));

                DAO da = new DAO(App.getContext());
                da.open();
                ArrayList<HashMap<String, String>> alst = da.getVOVisitBalacesByMember(datVisitDate, vono, memno);
                da.close();

                String[] from = new String[]{"[ProductSymbol]", "[LoanNo]", "[StName]", "[BALSYS]", "[BALPSS]"};
                int[] to = {R.id.textLSym, R.id.textDesc, R.id.textType, R.id.textYTR, R.id.textODAmt};

                SimpleAdapter ad = new SimpleAdapter(getApplicationContext(), alst,
                        R.layout.memberpf_row, from, to);
                lva.setAdapter(ad);

                HideButtonRows();
            }
        });

        lva.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                vmap = (HashMap<String, String>) lva.getItemAtPosition(position);
                String x = vmap.get("[BALPSS]");
                meditPassBall.setText(x);
                int rid = (vmap.get("[CLOC]").equals("0")? R.id.radioVO : R.id.radioOffice);
                //mradioLoc.setId((vmap.get("[CLOC]").equals("0")? R.id.radioVO : R.id.radioOffice));
                mradioLoc.check(rid);
                btnSave.setText("Matched");
                btnSave.setEnabled(true);
                btnDelete.setText("Unmatched");
                btnDelete.setEnabled(true);
//                meditPassBall.setEnabled(true);
//                meditPassBall.requestFocus();
                mrowRadio.setVisibility(View.VISIBLE);
                mrowButton.setVisibility(View.VISIBLE);
            }
        });

        meditPassBall.setText("");
        mradioLoc.clearCheck();
        btnSave.setEnabled(false);
        btnDelete.setEnabled(false);
        meditPassBall.setEnabled(false);

        HideButtonRows();

    }

    public void HideButtonRows() {
        meditPassBall.setText("");
        mradioLoc.clearCheck();
        btnSave.setText("Matched");
        btnDelete.setText("Unmatched");
        btnSave.setEnabled(false);
        btnDelete.setEnabled(false);
        //meditPassBall.setEnabled(false);
        mrowBalPrompt.setVisibility(View.GONE);
        mrowBalEdit.setVisibility(View.GONE);
        mrowRadio.setVisibility(View.INVISIBLE);
        mrowButton.setVisibility(View.INVISIBLE);
//        btnSave.setVisibility(View.GONE);
//        btnDelete.setVisibility(View.GONE);
//        mradioLoc.setVisibility(View.GONE);
    }

    public void onSave(View v) {
        String s = btnSave.getText().toString();
        Integer iSysBal = 0;
        if(s.equals("Matched")) {
            if (vmap.get("[BALSYS]").trim().length() > 0)
                iSysBal = Integer.parseInt(vmap.get("[BALSYS]").trim());
            meditPassBall.setText(iSysBal.toString());
        }
        if (P8.IsValidAmount(this, meditPassBall)) {
            Long sMemNo = Long.valueOf(memmap.get(DBHelper.FLD_ORG_MEM_NO));
            String sLN = vmap.get("[LoanNo]");
            Integer iLN = 0;
            if (!sLN.equals("Savings"))
                iLN = Integer.parseInt(sLN);
            String sPS = vmap.get("[ProductSymbol]");

            if (vmap.get("[BALSYS]").trim().length() > 0)
                iSysBal = Integer.parseInt(vmap.get("[BALSYS]").trim());
            Integer iPassBal = Integer.parseInt(meditPassBall.getText().toString());
            byte bLoc = (byte) ((mradioLoc.getCheckedRadioButtonId() == R.id.radioVO) ? 0: 1);
            double lat=0;
            double lon=0;

            DAO da = new DAO(this);
            da.open();
            boolean succ = da.UpdateVOBCheck(datVisitDate, vono, sMemNo, iLN, sPS, iSysBal, iPassBal, bLoc, lat, lon);
            da.close();

            if(succ) {
                vmap.put("[BALPSS]", iPassBal.toString());
//                memmap.put("HAS_VOVISIT_RECORD", "1");
                RefreshRecord();

////                lv.setAdapter(adapter);
//                lv.invalidate();
//                lv.refreshDrawableState();
//
//                SimpleAdapter ad = (SimpleAdapter) lva.getAdapter();
//                lva.setAdapter(ad);
//                lva.invalidate();
//                lva.refreshDrawableState();
//
//                refreshVOBCheckup();
//                HideButtonRows();
            }
        }
    }

    public void onDelete(View v) {
        String s = btnDelete.getText().toString();
        if(s.equals("Unmatched")) {
            btnSave.setText("Save");
            btnDelete.setText(("Delete"));
            mrowBalPrompt.setVisibility(View.VISIBLE);
            mrowBalEdit.setVisibility(View.VISIBLE);
            String x = vmap.get("[BALPSS]");
            meditPassBall.setText(x);
            meditPassBall.setEnabled(true);
            meditPassBall.requestFocus();
        }
        else {
            Long sMemNo = Long.valueOf(memmap.get(DBHelper.FLD_ORG_MEM_NO));
            String sLN = vmap.get("[LoanNo]");
            Integer iLN = 0;
            if (!sLN.equals("Savings"))
                iLN = Integer.parseInt(sLN);

            DAO da = new DAO(this);
            da.open();
            boolean succ = da.DeleteVOBCheck(datVisitDate, vono, sMemNo, iLN);
            da.close();

            if (succ) {
                vmap.put("[BALPSS]", "");
//                memmap.put("HAS_VOVISIT_RECORD", "0");  // Have to think about Auto generated (ramdomly selected) records
                RefreshRecord();
            }
        }
    }

    private void RefreshRecord() {
        Integer v = Integer.parseInt(memmap.get("HAS_VOVISIT_RECORD"));
        String  mk="0";
        HashMap<String, String> t;
        String s;
        for(int i = 0; i < lva.getCount(); i++) {
            t = (HashMap<String, String>) lva.getItemAtPosition(i);
            s = t.get("[BALPSS]");
            if(s.length() > 0) {
                mk = "1";
                break;
            }
        }
        memmap.put("HAS_VOVISIT_RECORD", mk);
//                lv.setAdapter(adapter);
        lv.invalidate();
        lv.refreshDrawableState();

        SimpleAdapter ad = (SimpleAdapter) lva.getAdapter();
        lva.setAdapter(ad);
        lva.invalidate();
        lva.refreshDrawableState();

        refreshVOBCheckup();
        HideButtonRows();
    }

    public void onCancel(View v) {
        finish();
    }

    public void onComments(View v) {
        Intent it =  new Intent(this, CommentsActivity.class);
        it.putExtra(CommentsActivity.CALLERNAME, getTitle().toString());
        it.putExtra(CommentsActivity.COMMENTSLABEL, "Passbook Check Comments");
        it.putExtra(CommentsActivity.COMMENTS, meditPBComments.getText().toString());
        startActivityForResult(it, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            String x = data.getStringExtra(CommentsActivity.COMMENTS).toString();
            meditPBComments.setText(x);
        }
    }
}
