package net.qsoft.brac.bmsmerp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LCReportBranchSum extends SSActivity implements OnItemClickListener {
    private static final String TAG = LCReportBranchSum.class.getSimpleName();
    static final String REPORT_LEVEL = LCReportBranchSum.class.getName();
    static final String START_DATE = REPORT_LEVEL + ".StartDate";
    static final String END_DATE = REPORT_LEVEL + ".EndDate";
    static final String PONO = REPORT_LEVEL + ".PONO";


    @BindView(R.id.textRPTHead) TextView mRPTHead;
    @BindView(R.id.textDateFrom) TextView mDateFrom;
    @BindView(R.id.textDateTo) TextView mDateTo;
    @BindView(R.id.textPONoH) TextView mPONoH;
    @BindView(R.id.okButton) Button cmdOK;
    @BindView(R.id.listViewLCRBS) ListView lv;

    private Integer repLevel = 0;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    Date datStart;
    Date datEnd;
    ArrayList<HashMap<String, String>> itms = null;
    String pono="";
    String poName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lcreport_branch_sum);
        ButterKnife.bind(this);

        cmdOK.setVisibility(View.GONE);

        if(getIntent().hasExtra(REPORT_LEVEL)) {
            repLevel = getIntent().getExtras().getInt(REPORT_LEVEL);
        }
        if(getIntent().hasExtra(END_DATE)) {
            datEnd = P8.ConvertStringToDate(getIntent().getExtras().getString(END_DATE), "dd-MM-yyyy");
        }
        else
            datEnd = P8.ToDay();

        Calendar newCal = Calendar.getInstance();

        if(getIntent().hasExtra(START_DATE)) {
            datStart = P8.ConvertStringToDate(getIntent().getExtras().getString(START_DATE), "dd-MM-yyyy");
        }
        else {
            newCal.setTime(datEnd);
            newCal.set(Calendar.DAY_OF_MONTH, 1);
            datStart = newCal.getTime();
        }

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datEnd = c.getTime();
                ShowReport();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));

//        newCal.set(Calendar.DAY_OF_MONTH, 1);
//        datStart = newCal.getTime();

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datStart = c.getTime();
               ShowReport();
//                StartNewReport(repLevel + 1);
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));

        if(repLevel == 1) {
            // Get PONo
            if(getIntent().hasExtra(PONO)) {
                pono = getIntent().getExtras().getString(PONO);
            }
            mPONoH.setText("VO No.\n");
        }
        else if(repLevel == 0)
            mPONoH.setText("PO/CO Name\n");
    }
    @Override
    protected void onResume() {
        super.onResume();
        ShowReport();
    }

    private void StartNewReport(int lvl, String cno) {
        Intent it = new Intent(App.getContext(), LCReportBranchSum.class);
        it.putExtra(REPORT_LEVEL, lvl)
                .putExtra(START_DATE, P8.FormatDate(datStart, "dd-MM-yyyy"))
                .putExtra(END_DATE, P8.FormatDate(datEnd, "dd-MM-yyyy"));
        if(!(cno == null || cno.trim().length()==0))
                it.putExtra(PONO, cno);
        startActivity(it);
    }

    private void ShowDateLabels() {
        mDateFrom.setText("From date: " + P8.FormatDate(datStart, "dd-MMM-yyyy"));
        mDateTo.setText("To: " + P8.FormatDate(datEnd, "dd-MMM-yyyy"));
    }

    protected void ShowReport() {
        if(repLevel==0) {
            mRPTHead.setText("Loan Repayment Collection\nBranch-wise Summary");

        }
//        else if(repLevel==2)
//            mRPTHead.setText("Loan Repayment Collection\nPO/CO-wise Details");

        ShowDateLabels();
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mDateFrom.getLayoutParams();
//        params.addRule(RelativeLayout.BELOW, mRPTHead.getId());
//        mDateFrom.setLayoutParams(params);

        DAO da = new DAO(this);
        da.open();
        if(repLevel ==  1) {
            poName = da.getCO(pono).get_COName();
            mRPTHead.setText("Loan Repayment Collection\nPO/CO Summary\n" + pono + " - " + poName);
        }
        itms = da.getLoanCollectionCOummary(datStart, datEnd, pono);
        da.close();

        String[] from = new String[] {DBHelper.FLD_CONAME, "[CurrAmt]", "[CurrBwr]", "[LateAmt]", "[LateBwr]",
                "[NIBL1Amt]", "[NIBL1Bwr]", "[TotalAmt]", "[TotalBwr]"};
        int[] to = {R.id.textPONo, R.id.textCLoanAmt, R.id.textCLoanBwr, R.id.textLLoanAmt, R.id.textLLoanBwr,
                R.id.textNLoanAmt, R.id.textNLoanBwr, R.id.textTLoanAmt, R.id.textTLoanBwr };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), itms, R.layout.lcreport_branch_sum_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mCONo = (TextView) v.findViewById(R.id.textPONo);
                String x = mCONo.getText().toString();
//                HashMap<String, String> row = (HashMap<String, String>) getItem(position);
                //if (row.get("[OrgMemNo]").equals("Total") || row.get("[OrgMemNo]").equals("Count")) {
                if(x.equals("Total") && (position == itms.size() - 1))  {
//                    v.setBackgroundResource(R.color.black_overlay);
                    mCONo.setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textCLoanAmt)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textCLoanBwr)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textLLoanAmt)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textLLoanBwr)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textNLoanAmt)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textNLoanBwr)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textTLoanAmt)).setBackgroundResource(R.color.black_overlay);
                    ((TextView) v.findViewById(R.id.textTLoanBwr)).setBackgroundResource(R.color.black_overlay);
                }
                else {
                    mCONo.setBackgroundResource(R.color.LightGreen);
                    ((TextView) v.findViewById(R.id.textCLoanAmt)).setBackgroundResource(R.color.AntiqueWhite);
                    ((TextView) v.findViewById(R.id.textCLoanBwr)).setBackgroundResource(R.color.AntiqueWhite);
                    ((TextView) v.findViewById(R.id.textLLoanAmt)).setBackgroundResource(R.color.NavajoWhite);
                    ((TextView) v.findViewById(R.id.textLLoanBwr)).setBackgroundResource(R.color.NavajoWhite);
                    ((TextView) v.findViewById(R.id.textNLoanAmt)).setBackgroundResource(R.color.Thistle);
                    ((TextView) v.findViewById(R.id.textNLoanBwr)).setBackgroundResource(R.color.Thistle);
                    ((TextView) v.findViewById(R.id.textTLoanAmt)).setBackgroundResource(R.color.AliceBlue);
                    ((TextView) v.findViewById(R.id.textTLoanBwr)).setBackgroundResource(R.color.AliceBlue);
                }

                return v;
            }
        };
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        lv.setClickable(true);
    }

    public void onDateClick(View v) {
        if(v.getId() == R.id.textDateFrom)
            fromDatePickerDialog.show();
        else if(v.getId() == R.id.textDateTo)
            toDatePickerDialog.show();
    }

    public void onCancel(View view) {
        // Back
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String cno = ((HashMap<String, String>) lv.getItemAtPosition(position)).get(DBHelper.FLD_CONO).toString();
        if(repLevel == 0) {
            if (!cno.equals("Total")) {
                StartNewReport(1, cno);
            }
        }
        if(repLevel == 1) {
            if (!cno.equals("Total")) {
                Long orgNo = Long.valueOf(((HashMap<String, String>) lv.getItemAtPosition(position)).get(DBHelper.FLD_CONAME).toString());
                Intent itn = new Intent(App.getContext(), LCReportVOSum.class);
                itn.putExtra(P8.VONO, orgNo)
                        .putExtra(START_DATE, P8.FormatDate(datStart, "yyyy-MM-dd"))
                        .putExtra(END_DATE, P8.FormatDate(datEnd, "yyyy-MM-dd"));
                startActivity(itn);
            }
        }
    }
}
