package net.qsoft.brac.bmsmerp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;

public class CommentsActivity extends SSActivity {
    private static final String TAG = CommentsActivity.class.getSimpleName();

    public static final String COMMENTS =  CommentsActivity.class.getSimpleName() + ".COMMENTS";
    public static final String COMMENTSLABEL =  CommentsActivity.class.getSimpleName() + ".COMMENTSLABEL";
    public static final String CALLERNAME =  CommentsActivity.class.getSimpleName() + ".CALLERNAME";

    TextView textCHeader;
    EditText editComments;
//    Button okButton;
//    Button cancelButton;

    //String strComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_comments);
        super.onCreate(savedInstanceState);

        textCHeader = (TextView) findViewById(R.id.textCHeader);
        editComments = (EditText) findViewById(R.id.editComments);
//        okButton = (Button) findViewById(R.id.okButton);
//        cancelButton = (Button) findViewById(R.id.cancelButton);

        Intent it = getIntent();
        if(it.hasExtra(CALLERNAME))
            getSupportActionBar().setTitle(it.getExtras().getString(CALLERNAME));
        if(it.hasExtra(COMMENTSLABEL))
            textCHeader.setText(it.getExtras().getString(COMMENTSLABEL));
        if(it.hasExtra((COMMENTS)))
            editComments.setText(it.getExtras().getString(COMMENTS));

    }

    public void onOk(View view) {
        Intent it = new Intent();
        it.putExtra(COMMENTS, editComments.getText().toString());
        setResult(RESULT_OK, it);
        finish();
    }

    public void onCancel(View view) {
        // TODO Auto-generated method stub
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        onOk(null);
    }
}
