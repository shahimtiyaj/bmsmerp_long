package net.qsoft.brac.bmsmerp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class VOResultActivity extends VOActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		lv.setOnItemLongClickListener(null);
		lv.setLongClickable(false);
	}
	
	@Override
	protected void startAction(Integer opt, Long vono) {
		if(opt==2) {
			// Return vonumber
			
			Intent it = new Intent();
			it.putExtra(P8.VONO, vono);
        	setResult(RESULT_OK, it);
    		finish();
		}
	}
	
	@Override
	public void onCancel(View view) {
		// TODO Auto-generated method stub
    	setResult(RESULT_CANCELED);
		finish();
	}
}
