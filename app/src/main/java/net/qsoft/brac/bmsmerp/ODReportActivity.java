package net.qsoft.brac.bmsmerp;

import java.util.ArrayList;
import java.util.HashMap;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.PO;
import net.qsoft.brac.bmsmerp.data.VO;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ODReportActivity extends SSActivity {
	private static final String TAG = ODReportActivity.class.getSimpleName();
	
	ListView lv = null;
	ArrayList<HashMap<String, String>> items=null;
	Long vono=null;
	Button cmdOK;
	TextView branchName;
	TextView voName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_odreport);

		cmdOK = (Button) findViewById(R.id.okButton);
		cmdOK.setVisibility(View.GONE);
		branchName = (TextView) findViewById(R.id.textBranchName);
		voName = (TextView) findViewById(R.id.textVOName);
		
		lv = (ListView) findViewById(R.id.listViewORD);
		
		if(getIntent().hasExtra(P8.VONO)) { 
			vono = getIntent().getExtras().getLong(P8.VONO,0);
			Log.d(TAG, "In has extra " + vono);
		} 

		createODReportList(vono);
	}
	
	private void createODReportList(Long von) {
//		Log.d(TAG, "VO No.: " + (von==null ? "": von));
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		VO vo = da.getVO(von);
		items = da.getOverDueByVO(von); 
//		Log.d(TAG, "Items: " + items );
		da.close();
		
		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
		voName.setText(von + " - " + vo.get_OrgName());
		
		ODRListAdapter adapter = new ODRListAdapter(this, R.layout.ordreport_row, items); 
		Log.d(TAG, "Array list addapter: " + adapter);
		lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	

}
