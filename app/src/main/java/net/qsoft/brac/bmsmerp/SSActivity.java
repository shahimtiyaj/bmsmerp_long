package net.qsoft.brac.bmsmerp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;

import net.qsoft.brac.bmsm.R;

@SuppressLint("Registered")
public class SSActivity extends AppCompatActivity {
	Toolbar toolbar = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		toolbar = (Toolbar) findViewById(R.id.tool_bar);

		if(toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setTitle(getTitle());
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setIcon(R.mipmap.ic_launcher);
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onKeyLongPress(int, android.view.KeyEvent)
	 */
//	@Override
//	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
//		// TODO Auto-generated method stub
//		 if(keyCode == KeyEvent.KEYCODE_HOME){
//			return true;
//		}
//		return super.onKeyLongPress(keyCode, event);
//	}

	
}
