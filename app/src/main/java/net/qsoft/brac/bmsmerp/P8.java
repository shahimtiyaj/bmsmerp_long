package net.qsoft.brac.bmsmerp;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;

public class P8 {

    private static final String TAG = P8.class.getSimpleName();

    public static final String TRANS_OPTION = "P8.TRANSACTION_OPTION";
    public static final int TRANS_OPTION_SAVINGS = 0;
    public static final int TRANS_OPTION_REPAYMENT = 1;
    public static final String BCHECK_BALANCE = "net.qsoft.brac.bmsm.BCHECK_BALANCE";
    public static final String DBITEMS = "P8.DBITEMS";
    public static final String DDATE = "P8.DDATE";
    public static final String VONO = "P8.VONO";
    public static final String PONO = "P8.PONO";
    public static final String MEMNO = "P8.MEMNO";
    public static final String LOANNO = "P8.LOANNO";
    public static final String MEMLIST = "P8.MEMLIST";
    //	public static final String CLIENTINFO ="org.safesave.p8acm.P8.CLIENTINFO";
//	public static final String ACCOUNTNO="org.safesave.p8acm.P8.ACCOUNTNO";
    public static final String COLLNO = "P8.COLLNO";
    //	public static final String TRANSACTION_TYPE="org.safesave.p8acm.P8.TRANSACTION_TYPE";
    public static final String CONFIG = "P8.CONFIG";
    public static final String SYNC_SERVER_NAME = "sync_server_name";
    public static final String SYNC_SERVER_IP = "sync_server_ip";
    public static final String SYNC_SERVER_PORT = "sync_server_port";
    public static final String PREF_HAS_PRINT_MODULE = "pref_has_print_module";

    // Bluetooth printer
    public static final int MESSAGE_STATE_CHANGE = 100;
    public static final int MESSAGE_READ = 101;
    public static final int MESSAGE_WRITE = 102;
    public static final int MESSAGE_DEVICE_NAME = 110;
    public static final int MESSAGE_TOAST = 111;

    public static final String DEVICE_NAME = "P8.DEVICE_BAME";
    public static final String TOAST = "P8.TOAST";
    // End of Blutooth printer

    private P8() {

    }

    public static void PlayRawResource(final Context ctx, final String fname) {
        Thread t = new Thread() {
            public void run() {
                int resID = ctx.getResources().getIdentifier(fname, "raw", ctx.getPackageName());
                MediaPlayer player = MediaPlayer.create(ctx, resID);
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        mp.release();
                    }
                });
                player.start();
            }
        };
        t.start();
    }

    public static void ErrorSound(Context ctx) {
        PlayRawResource(ctx, "error");
    }

    public static void OkSound(Context ctx) {
        PlayRawResource(ctx, "ok");
    }

    public static void ShowError(Context context, String errText) {
        ErrorSound(context);
        AlertDialog myAlertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_showerror);
        builder.setMessage(errText);
        builder.setIcon(android.R.drawable.ic_dialog_alert); // R.drawable.stop
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        myAlertDialog = builder.create();
        myAlertDialog.show();
    }

    public static void ShowMessage(Context context, String text, String title, int iCon) {
        AlertDialog myAlertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(text);
        builder.setIcon(iCon); // R.drawable.stop
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        myAlertDialog = builder.create();
        myAlertDialog.show();
    }

    public static int MessageBox(Context context, String msg, int iTitle, int iYes, int iNo, int iICon) {
        final Integer[] ret = new Integer[1];
        ret[1] = 0;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ret[0] = which;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(iTitle)
                .setMessage(msg)
                .setIcon(iICon)
                .setCancelable(false)
                .setPositiveButton(iYes, dialogClickListener)
                .setNegativeButton(iNo, dialogClickListener).show();

        synchronized (context) {
            try {
                context.wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                Log.d(TAG, e.getLocalizedMessage());
            }
        }
        return ret[0];
    }

    public static Boolean BalanceEqual(Context context, int bal, int ent, String itm) {
        boolean ret = false;

        if (bal == ent) {
            ErrorSound(context);
            ret = (MessageBox(context,
                    itm + context.getString(R.string.prompt_balance_equal),
                    R.string.title_warrning_dialog,
                    android.R.string.yes,
                    android.R.string.no,
                    android.R.drawable.ic_dialog_alert) == DialogInterface.BUTTON_POSITIVE);
        } else
            ret = true;

        return ret;
    }

    public static boolean IsValidAmount(Context context, EditText editAmount) {
        boolean ret = true;
        try {
            if (editAmount.getText().toString().trim().length() == 0) {
                ErrorSound(context);
                editAmount.setError(context.getString(R.string.error_valid_number));
                ret = false;
            } else if (Integer.parseInt(editAmount.getText().toString()) < 0) {
                //Error 0 or negative number not allowed
                ErrorSound(context);
                editAmount.setError(context.getString(R.string.error_zero_negative_number));
                ret = false;
            }
        } catch (Exception ex) {
            ErrorSound(context);
            editAmount.setError(ex.getMessage() + ": " + context.getString(R.string.error_valid_number));
            ret = false;
        }
        if (!ret)
            editAmount.requestFocus();
        return ret;
    }

    public static boolean IsValidVoucher(Context context, EditText editVoucher) {
        if (editVoucher.getText().toString().trim().length() == 0) {
            ErrorSound(context);
            editVoucher.setError(context.getString(R.string.error_voucher_required));
            editVoucher.requestFocus();
//    		ShowError(context, context.getString(R.string.error_voucher_required));
            return false;
        } else
            return true;
    }

    public static String getDateTime() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return getDateTime(date);
    }

    public static String getDateTime(Date dt) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(dt);
    }

    public static String getDate() {
        return DateFormat.getDateInstance().format(new Date());
    }

    public static Date ToDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static Date ConvertStringToDate(String dateString, String formatString) {
        Date ret = null;

        try {
            ret = (new SimpleDateFormat(formatString, Locale.getDefault())).parse(dateString);
//    		ret = (new SimpleDateFormat(formatString, Locale.ENGLISH)).parse(dateString);
        } catch (ParseException e) {
            Log.e(TAG, e.toString());
        }
        return ret;
    }

    public static Date ConvertStringToDate(String dateString) {
        return ConvertStringToDate(dateString, "yyyy-MM-dd hh:mm:ss");
    }

    public static String FormatDate(Date date, String formatString) {
       // return (new SimpleDateFormat(formatString, Locale.getDefault())).format(date);
        String s=null;
        try {
             s= new SimpleDateFormat(formatString, Locale.getDefault()).format(date);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return s;
    }

    public static String PathCombine(String path1, String path2) {
        File file1 = new File(path1);
        File file2 = new File(file1, path2);
        return file2.getPath();
    }

    // pad with " " to the right to the given length (n)
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    // pad with " " to the left to the given length (n)
    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    // pad with " " to the left to the given length (n)
    public static String LeftPad(String s, int n) {
        return String.format("%0" + n + "d", Integer.parseInt(s));
    }

    public static int daysBetweenDates(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return (int) (diff / (1000 * 60 * 60 * 24));
    }

    public static void SaveSyncTime() {
        DAO da = new DAO(App.getContext());
        da.open();
        da.WriteSyncTime(getDateTime());
        da.close();
    }

    public static Date getLastSyncTime() {
        Date r;

        DAO da = new DAO(App.getContext());
        da.open();
        String dt = da.GetSyncTime();
        da.close();

        if (dt != null && !dt.isEmpty())
            r = ConvertStringToDate(dt, "yyyy-MM-dd HH:mm:ss");
        else
            r = ConvertStringToDate("2000-01-01", "yyyy-MM-dd");

        return r;
    }

    public static String getPOCondition(String cono) {
        return (cono == null || cono.isEmpty()) ? "%" : padLeft(cono, 8);
    }

/*    public static String getVOCondition(Long vono) {

       return String.valueOf((vono == null || vono==0) ? "%" : vono);
    }*/


    public static void NotImplemented() {
        Toast.makeText(App.getContext(), "Function not implemented yet", Toast.LENGTH_SHORT).show();
    }

    public static void NotImplemented2() {
        Toast.makeText(App.getContext(), "Coming soon", Toast.LENGTH_SHORT).show();
    }

    /*
    public  static void ShowNotification(Context context, Drawable ic, String title, String text) {
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(drawable)
                            .setContentTitle(title)
                            .setContentText(text);

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);

            // Add as notification
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());
        }
    }
    */
    static String banglanumerals = "০১২৩৪৫৬৭৮৯";

    public static String toBanglaNumber(Integer num) {
        return toBanglaNumber(num.toString());
    }

    public static String toBanglaNumber(String s) {
        StringBuilder sb = new StringBuilder();

        for (char c : s.trim().toCharArray()) {
            if (c >= 48 && c <= 57) {
                int i = c - 48;
                sb.append(banglanumerals.charAt(i));
            } else
                sb.append(c);
        }
        return sb.toString();
    }


    public static void showProgressbar(Context ctx, String message) {
        new SpotsDialog.Builder()
                .setContext(ctx).setMessage(message)
                .build()
                .show();
    }

    public static void dismissProgressbar(Context ctx) {
        new SpotsDialog.Builder()
                .setContext(ctx)
                .build()
                .dismiss();
    }

}
