package net.qsoft.brac.bmsmerp.data;

import net.qsoft.brac.bmsmerp.P8;

import java.util.Date;

public class CSMBModel {
    private String TerritoryName;
    private Long OrgNo;
    private Long OrgMemNo;
    private String MemberName;
    private String LoanNo;
    private Date TargetDate;
    private Date CollDate;
    private int TargetAmount;
    private int CollAmount;
    private String PhoneNo;

    public CSMBModel() {

    }

    public CSMBModel(String territoryName, Long orgNo, Long orgMemNo, String memberName,
                     String loanNo, String targetDate, String collDate, int targetAmount,
                     int collAmount, String phoneNo) {
    }


    public String getTerritoryName() {
        return TerritoryName;
    }

    public void setTerritoryName(String territoryName) {
        TerritoryName = territoryName;
    }

    public Long getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(Long orgNo) {
        OrgNo = orgNo;
    }

    public Long getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(Long orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public Date getTargetDate() {
        return TargetDate;
    }

    public void setTargetDate(String targetDate) {
        if(targetDate != null && targetDate.trim().length() > 0)
            TargetDate = P8.ConvertStringToDate(targetDate, "yyyy-MM-dd");
        else
            TargetDate = null;  //P8.ConvertStringToDate("1901-01-01", "yyyy-MM-dd");
    }

    public Date getCollDate() {
        return CollDate;
    }

    public void setCollDate(String collDate) {
        if(collDate != null && collDate.trim().length() > 0)
            CollDate = P8.ConvertStringToDate(collDate, "yyyy-MM-dd");
        else
            CollDate = null;    //P8.ConvertStringToDate("1901-01-01", "yyyy-MM-dd");
    }

    public int getTargetAmount() {
        return TargetAmount;
    }

    public void setTargetAmount(int targetAmount) {
        TargetAmount = targetAmount;
    }

    public int getCollAmount() {
        return CollAmount;
    }

    public void setCollAmount(int collAmount) {
        CollAmount = collAmount;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}
