package net.qsoft.brac.bmsmerp;

import java.util.List;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.VO;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class VOActivity extends SSActivity {
	private static final String TAG = VOActivity.class.getSimpleName();
	
	ListView lv = null;
	List<VO> items=null;

	String pono = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_vo);
		super.onCreate(savedInstanceState);

		lv = (ListView) findViewById(R.id.lstView);

		if(getIntent().hasExtra(P8.PONO)) {
			pono = getIntent().getExtras().getString(P8.PONO);
			getSupportActionBar().setTitle("VO List for PO/CO: " + pono);
		}
		DAO da = new DAO(this);
		da.open();
		items = da.getVOList(pono);
		da.close();
		
		VOListAdapter adapter = new VOListAdapter(this);
		adapter.addSectionHeaderItem("Todays VO");
		for(VO vo:items) {
			if(vo.isTodaysVO())
				adapter.addItem(vo);
		}
		
		adapter.addSectionHeaderItem("All Other VOs");
		for(VO vo:items) {
			if(!vo.isTodaysVO())
				adapter.addItem(vo);
		}
		
		lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	startAction(2, ((VO) ((VOListAdapter.DataHolder) lv.getItemAtPosition(position)).vo).get_OrgNo());
            	
//                String item =  ((DataHolder) lv.getItemAtPosition(position)).Label; // .get("Id")) ;
//                //startAction(item);                
//                Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            }
        });
		
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				startAction(1, ((VO) ((VOListAdapter.DataHolder) lv.getItemAtPosition(position)).vo).get_OrgNo());

	                //startAction(item);                
				//Toast.makeText(getBaseContext(), "Long pressed: " + item , Toast.LENGTH_LONG).show();
	                
				return true;
			}
		});
		
		lv.setAdapter(adapter);
	}

	protected void startAction(Integer opt, Long vono) {
		if(opt==1) {
			// VO Summary
			Intent it = new Intent(this, TTActivity.class);
			it.putExtra(P8.VONO, vono);
//			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(it);
//			finish();
		}
		else if(opt==2) {
			// Member list
			Intent it = new Intent(this, ClrMemberActivity.class);
			it.putExtra(P8.DBITEMS, DAO.DBoardItem.MEMBERS.name());
			it.putExtra(P8.VONO, vono);
			startActivity(it);
		}
	}
		
	public void onCancel(View view) {
		// Back
		finish();
	}	
}
