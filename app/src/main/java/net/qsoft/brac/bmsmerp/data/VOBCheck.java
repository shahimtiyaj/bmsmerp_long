package net.qsoft.brac.bmsmerp.data;

import java.util.Date;

/**
 * Created by zferdaus on 2/18/2017.
 */

public class VOBCheck {
    Integer _id;
    Date _VisitDate;
    String _OrgNo;
    String _OrgMemNo;
    String _ProductSymbol;
    Integer _LoanNo;
    Integer _Balance;
    Integer _PassBook;
    Byte _CLoc;
    double _lat;
    double _lon;

    public VOBCheck(Integer _id, Date _VisitDate, String _OrgNo, String _OrgMemNo, String _ProductSymbol, Integer _LoanNo, Integer _Balance, Integer _PassBook, Byte _CLoc, double _lat, double _lon) {
        this._id = _id;
        this._VisitDate = _VisitDate;
        this._OrgNo = _OrgNo;
        this._OrgMemNo = _OrgMemNo;
        this._LoanNo = _LoanNo;
        this._Balance = _Balance;
        this._PassBook = _PassBook;
        this._CLoc = _CLoc;
        this._lat = _lat;
        this._lon = _lon;
    }

    public VOBCheck() {
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public Date get_VisitDate() {
        return _VisitDate;
    }

    public void set_VisitDate(Date _VisitDate) {
        this._VisitDate = _VisitDate;
    }

    public String get_OrgNo() {
        return _OrgNo;
    }

    public void set_OrgNo(String _OrgNo) {
        this._OrgNo = _OrgNo;
    }

    public String get_OrgMemNo() {
        return _OrgMemNo;
    }

    public void set_OrgMemNo(String _OrgMemNo) {
        this._OrgMemNo = _OrgMemNo;
    }

    public Integer get_LoanNo() {
        return _LoanNo;
    }

    public String get_ProductSymbol() {
        return _ProductSymbol;
    }

    public void set_ProductSymbol(String _ProductSymbol) {
        this._ProductSymbol = _ProductSymbol;
    }

    public void set_LoanNo(Integer _LoanNo) {
        this._LoanNo = _LoanNo;
    }

    public Integer get_Balance() {
        return _Balance;
    }

    public void set_Balance(Integer _Balance) {
        this._Balance = _Balance;
    }

    public Integer get_PassBook() {
        return _PassBook;
    }

    public void set_PassBook(Integer _PassBook) {
        this._PassBook = _PassBook;
    }

    public Byte get_CLoc() {
        return _CLoc;
    }

    public void set_CLoc(Byte _CLoc) {
        this._CLoc = _CLoc;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lon() {
        return _lon;
    }

    public void set_lon(double _lon) {
        this._lon = _lon;
    }
}
