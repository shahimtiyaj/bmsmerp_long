package net.qsoft.brac.bmsmerp;

import java.util.ArrayList;
import java.util.HashMap;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DAO.DBoardItem;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.annotation.SuppressLint;
import android.content.Intent;

public class TTActivity extends SSActivity  { 
	private static final String TAG = TTActivity.class.getSimpleName();
	
	ListView lv = null;
	ArrayList<HashMap<String, String>> items=null;
	Long vono = null;
	String pono = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_tt);
		super.onCreate(savedInstanceState);

		lv = (ListView) findViewById(R.id.lstView);
		if(getIntent().hasExtra(P8.VONO)) { 
			vono = getIntent().getExtras().getLong(P8.VONO,0);
//			pono = null;
			Log.d(TAG, "In has extra " + vono);
			getSupportActionBar().setTitle("VO Summary VO No.: " + vono);
//			setTitle("VO Summary VO No.: " + vono);
		}
		else if(getIntent().hasExtra(P8.PONO)) {
			// PO Dashboard selected
			pono = getIntent().getExtras().getString(P8.PONO);
//			vono = null;
			getSupportActionBar().setTitle("PO Dashboard: " + pono);
		}
		lv.setOnItemClickListener(new OnItemClickListener() {
            @SuppressWarnings("unchecked")
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                
                try {
					DBoardItem item =  DBoardItem.valueOf(((HashMap<String, String>) lv.getItemAtPosition(position)).get("Id")) ;
					startAction(item);
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}                
                //Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            }
        });
		
		createDashBoard(vono, pono);
	}

	private void createDashBoard(Long von, String pon) {
		Log.d(TAG, "No.: " + (von==null && pon==null ? "":   pon + von));
		DAO da = new DAO(this);
		da.open();
		items = da.getDashboardItems(von, pon);
		da.close();
		
		DBoardAdapter adapter = new DBoardAdapter(this, R.layout.dashboard_row, items); 
		lv.setAdapter(adapter);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onNewIntent(android.content.Intent)
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		if(getIntent().hasExtra(P8.VONO)) {
			vono = getIntent().getExtras().getLong(P8.VONO,0);
	//		pono = null;
		}
		else if(getIntent().hasExtra(P8.PONO)) {
			// PO Dashboard selected
			pono = getIntent().getExtras().getString(P8.PONO);
	//		vono = null;
		}
		createDashBoard(vono, pono);
	}

	private void startAction(DBoardItem itm) {
		Intent it;

        switch(itm) {
		case COS:
			startActivity (new Intent(this, COListActivity.class));
			break;

        case VOS:
			it = new Intent(this, VOActivity.class);
			if(!(pono == null || pono.isEmpty()))
				it.putExtra(P8.PONO, pono);
			startActivity (it);
			break;
			
        case MEMBERS:
        case BORROWERS:
        case CURBORROWERS:
        case LOANS:
        case CURLOANS:
        case L1LOANS:
        case L2LOANS:
		case N1LOANS:
		case N2LOANS:
		case OUTLOANS:
		case OVERLOANS:
		case SAVE_TARGET:
		case SAVE_YET_COLLECTED:
		case REPAY_TARGET:
		case REPAY_YET_COLLECTED:

        	it = new Intent(this, ClrMemberActivity.class);
        	it.putExtra(P8.DBITEMS, itm.name());
        	if(vono != null)
        		it.putExtra(P8.VONO, vono);
			else if(pono != null)
				it.putExtra(P8.PONO, pono);
			startActivity(it);
			break;
			
		case PO_CASH_IN_HAND:
			if((vono==null || vono==0 ) &&
					(pono==null || pono.trim().length()==0)) {
				// Only BM's own collection detail
				it = new Intent(this, CListActivity.class);
				if (vono != null)
					it.putExtra(P8.VONO, vono);
				else if (pono != null)
					it.putExtra(P8.PONO, pono);
				startActivity(it);
			}
			else if((pono!=null && pono.trim().length() > 0)) {
				// PO collection summary
				it = new Intent(this, CListSummaryActivity.class);
				it.putExtra(P8.PONO, pono);
				startActivity(it);
			}
			break;
			
        }
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		createDashBoard(vono, pono);
	}

/*	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	      super.onCreateContextMenu(menu, v, menuInfo);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.dboard_menu, menu);
	} */

	/* (non-Javadoc)
	 * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
	 */
/*	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent it;
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    HashMap<String, String>  temp = (HashMap<String, String>) lv.getItemAtPosition(info.position);
	    String _desc = temp.get("Desc");
	      
	    switch(item.getItemId()) {
	    case R.id.menu_vo_members:
	    	
            Toast.makeText(getBaseContext(), "VO Members: " + _desc, Toast.LENGTH_LONG).show();
	    	return true;
	    	  
	    case R.id.menu_vo_summary:
	    	
	    	Toast.makeText(getBaseContext(), "VO Summary: " + _desc, Toast.LENGTH_LONG).show();
	    	return true;
	    }
	    
		return super.onContextItemSelected(item);
	} */
	
}
