package net.qsoft.brac.bmsmerp.data;

import java.util.Date;
import java.util.HashMap;

import net.qsoft.brac.bmsmerp.App;
import net.qsoft.brac.bmsmerp.P8;

public class VO {
	private static final String TAG = VO.class.getSimpleName();

	static HashMap<String, String> COs = new HashMap<String, String>();

	private Long _OrgNo;
	private String _OrgName;
	private String _OrgCategory;
	private Integer _MemSexID;
	private Integer _SavColcOption;
	private Integer _LoanColcOption;
	private Date _SavColcDate;
	private Date _LoanColcDate;
	private Date _TargetDate;
	private Integer _Period;
	private String _CONo;
	private String _TerritoryName;

	/**
	 * @param _OrgNo
	 * @param _OrgName
	 * @param _OrgCategory
	 * @param _MemSexID
	 * @param _SavColcOption
	 * @param _LoanColcOption
	 * @param _SavColcDate
	 * @param _LoanColcDate
	 */
	public VO(Long _OrgNo, String _OrgName, String _OrgCategory,
			Integer _MemSexID, Integer _SavColcOption, Integer _LoanColcOption,
			Date _SavColcDate, Date _LoanColcDate, String _CONo, String _TerritoryName) {
		super();
		this._OrgNo = _OrgNo;
		this._OrgName = _OrgName;
		this._OrgCategory = _OrgCategory;
		this._MemSexID = _MemSexID;
		this._SavColcOption = _SavColcOption;
		this._LoanColcOption = _LoanColcOption;
		this._SavColcDate = _SavColcDate;
		this._LoanColcDate = _LoanColcDate;
		this._TargetDate = _LoanColcDate;
		this._Period = 0;
		this._CONo = _CONo;
		this._TerritoryName = _TerritoryName;

		if(!COs.containsKey(_CONo)) {
			// Retrieve
			DAO da = new DAO(App.getContext());
			da.open();
			CO co = da.getCO(_CONo);
			da.close();
			COs.put(_CONo, co.get_COName());
		}
	}


	public VO(Long _OrgNo, String _OrgName, String _OrgCategory,
			  Integer _MemSexID, Integer _SavColcOption, Integer _LoanColcOption,
			  Date _SavColcDate, Date _LoanColcDate, Date _TargetDate) {
		super();
		this._OrgNo = _OrgNo;
		this._OrgName = _OrgName;
		this._OrgCategory = _OrgCategory;
		this._MemSexID = _MemSexID;
		this._SavColcOption = _SavColcOption;
		this._LoanColcOption = _LoanColcOption;
		this._SavColcDate = _SavColcDate;
		this._LoanColcDate = _LoanColcDate;
		this._TargetDate = _TargetDate;
		this._Period = 0;

	}

	public  final String get_CONo() {
		return _CONo;
	}

	public  final String get_COName() {
		return COs.get(_CONo);
	}

	/**
	 * @return the _OrgNo
	 */
	public final Long get_OrgNo() {
		return _OrgNo;
	}
	/**
	 * @return the _OrgName
	 */
	public final String get_OrgName() {
		return _OrgName;
	}
	/**
	 * @return the _OrgCategory
	 */
	public final String get_OrgCategory() {
		return _OrgCategory;
	}
	/**
	 * @return the _MemSexID
	 */
	public final Integer get_MemSexID() {
		return _MemSexID;
	}
	/**
	 * @return the _SavColcOption
	 */
	public final Integer get_SavColcOption() {
		return _SavColcOption;
	}
	/**
	 * @return the _LoanColcOption
	 */
	public final Integer get_LoanColcOption() {
		return _LoanColcOption;
	}
	/**
	 * @return the _SavColcDate
	 */
	public final Date get_SavColcDate() {
		return _SavColcDate;
	}
	/**
	 * @return the _LoanColcDate
	 */
	public final Date get_LoanColcDate() {
		return _LoanColcDate;
	}
	
	public final Date get_TargetDate() {
		return _TargetDate;
	}
	
	public final Integer get_Period() {
		return _Period;
	}

	public String get_TerritoryName() {
		return _TerritoryName;
	}


/*	public final void set_Today(Date newDate) {
		this._Today = newDate;
	}
	
	public final Date get_Today() {
		return _Today;
	} */
	
	public final boolean isTodaysVO() {
//		Log.d(TAG, get_OrgNo()+ " Target: " + get_TargetDate());
//		Log.d(TAG, get_OrgNo()+ " Today: " + P8.ToDay());
		return  P8.FormatDate(get_TargetDate(), "yyyyMMdd").equals(P8.FormatDate(P8.ToDay(), "yyyyMMdd"));
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return get_OrgNo() + " - " + get_OrgName();
	}
	
	
}
