package net.qsoft.brac.bmsmerp.data;

import java.util.Date;

/**
 * Created by zferdaus on 5/6/2017.
 */

public class Followup {
    Integer _id;
    Date _FDate;
    Integer _FCType;
    String _Comment;
    Integer _DataSent;

    public Followup(Integer _id, Date _FDate, Integer _FCType, String _Comment, Integer _DataSent) {
        this._id = _id;
        this._FDate = _FDate;
        this._FCType = _FCType;
        this._Comment = _Comment;
        this._DataSent = _DataSent;
    }

    public Integer get_id() {
        return _id;
    }

    public Date get_FDate() {
        return _FDate;
    }

    public Integer get_FCType() {
        return _FCType;
    }

    public String get_Comment() {
        return _Comment;
    }

    public Integer get_DataSent() {
        return _DataSent;
    }
}
