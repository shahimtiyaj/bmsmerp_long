package net.qsoft.brac.bmsmerp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class CSCheckInfo extends SSActivity {
    private static final String TAG = CSCheckInfo.class.getSimpleName();

    TextView mDate;
    ListView lv;
    ListView lvEv;

    Date datDate;
    private DatePickerDialog fromDatePickerDialog;

    ArrayList<HashMap<String, String>> itms = null;
    ArrayList<HashMap<String, String>> itmsEv = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_cscheck_info);
        super.onCreate(savedInstanceState);

        Button okButton = (Button) findViewById(R.id.okButton);
        okButton.setVisibility(View.GONE);

        mDate = (TextView) findViewById(R.id.textDate);
        lv = (ListView) findViewById(R.id.listViewCSChek);
        lvEv = (ListView) findViewById(R.id.listViewCSChekEv);

        datDate = P8.ToDay();
        Calendar newCal = Calendar.getInstance();
        newCal.setTime(datDate);

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datDate = c.getTime();
                ShowReport();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowReport();
    }

    private void ShowReport() {
        mDate.setText(P8.FormatDate(datDate, "dd-MMM-yyyy"));

        DAO da = new DAO(this);
        da.open();
        itms = da.getCSCheckInfoReport(datDate);
        itmsEv = da.getCSCheckInfoEvReport(datDate);
        da.close();

        String[] from = new String[]{DBHelper.FLD_CONAME, DBHelper.FLD_ORG_NO, DBHelper.FLD_TOT_CURR_BORROWER ,
                DBHelper.FLD_MIS_INSTL_NUMB , DBHelper.FLD_MIS_INSTL_AMNT, DBHelper.FLD_ODREAL};
        int[] to = {R.id.textPONam, R.id.textVONo, R.id.textTotCurBor, R.id.textMissedNo, R.id.textMissedAmt, R.id.textODReal};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), itms, R.layout.cscheck_info_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mPOName = (TextView) v.findViewById(R.id.textPONam);
                String x = mPOName.getText().toString();
//                HashMap<String, String> row = (HashMap<String, String>) getItem(position);
                //if (row.get("[OrgMemNo]").equals("Total") || row.get("[OrgMemNo]").equals("Count")) {
                if (x.equals("Total")) {
                    //v.setBackgroundResource(R.drawable.cust_border);
                    v.setBackgroundResource(R.color.black_overlay);
                }
                return v;
            }
        };
        lv.setAdapter(adapter);

        String[] fromEv = new String[]{DBHelper.FLD_CONAME, DBHelper.FLD_ORG_NO, DBHelper.FLD_ODCUR_NO ,
                DBHelper.FLD_ODCUR_AMT, DBHelper.FLD_ODLAT_NO, DBHelper.FLD_ODLAT_AMT, DBHelper.FLD_ODNIBL_AMT, DBHelper.FLD_ODTOT_AMT};
        int[] toEv = {R.id.textPONamOD, R.id.textVONoOD, R.id.textCurrentNumOD, R.id.textCurrentAmtOD,
                R.id.textLateNumOD, R.id.textLateAmtOD, R.id.textNIBLOD, R.id.textTotalOD};

        SimpleAdapter adapterEv = new SimpleAdapter(getApplicationContext(), itmsEv, R.layout.cscheck_info_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mPOName = (TextView) v.findViewById(R.id.textPONamOD);
                String x = mPOName.getText().toString();
                if (x.equals("Total")) {
                    v.setBackgroundResource(R.color.black_overlay);
                }
                return v;
            }
        };
        lvEv.setAdapter(adapterEv);
    }

    public void onDateClick(View v) {
        if (v.getId() == R.id.textDate)
            fromDatePickerDialog.show();
    }

    public void onCancel(View v) {
        finish();
    }
}