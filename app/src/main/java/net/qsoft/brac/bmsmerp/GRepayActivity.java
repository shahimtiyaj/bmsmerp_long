package net.qsoft.brac.bmsmerp;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CLoan;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

public class GRepayActivity extends TBaseActivity {

	private DatePickerDialog fromDatePickerDialog;

	TextView balLabel;

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		TType="L";
		setTitle(getString(R.string.description_repay));
//		imageIcon.setImageResource(R.drawable.repaid);
//		imageIcon.setContentDescription(getString(R.string.description_repay));
		instLabel.setVisibility(View.VISIBLE);
		repoButton.setVisibility(View.VISIBLE);
		findViewById(R.id.futureButton).setVisibility(View.VISIBLE);
		balLabel = (TextView) findViewById(R.id.balLabel);
		balLabel.setVisibility(View.VISIBLE);

		Calendar newCalendar = Calendar.getInstance();
		fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);

				CLoan cl = CLoan.get_lastCL();
				Calendar c = Calendar.getInstance();
				c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
				Integer bal = cl.get_LastLB(c.getTime());

				//balLabel.setText("Loan balance on " + P8.FormatDate(c.getTime(), "MMM dd, yyyy") + ": " +  bal.toString());
				P8.ShowMessage(GRepayActivity.this,
						"Loan balance on " + P8.FormatDate(c.getTime(), "MMM dd, yyyy") + " ==>  " +  String.format("%,d", bal),
						"Loan balance on a date", R.mipmap.ic_calculator);

//				Snackbar snackbar = Snackbar.make(repoButton,
//						"Loan balance on " + P8.FormatDate(c.getTime(), "MMM dd, yyyy") + ": " +  bal.toString(),
//						Snackbar.LENGTH_LONG);
//				snackbar.show();
			}
		}, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
	}

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		cl = CLoan.get_lastCL();
		dispLabel.setText("Loan bal: " + cl.get_LastLB(P8.ToDay()).toString());	//		cl.get_LB().toString());
		dispLabel2.setText("Target: " + cl.get_IAB().toString());
		accLabel.setText(getString(R.string.prompt_account_no) + cl.get_LoanNo().toString());
		instLabel.setText("Inst. passed: " + cl.get_InstlPassed().toString());

/*		if((P8.daysBetweenDates(cl.get_DisbDate(), P8.ToDay()) + 1) <= 15) {
			okButton.setVisibility(android.view.View.GONE);
			editAmount.setFocusable(false);
			editAmount.setEnabled(false);
			editAmount.setHintTextColor(android.graphics.Color.rgb(0xFF, 0, 0));
			editAmount.setHint("LOAN IN GRACE PERIOD");
		} */
	}

	public void onOk(View view) {
		if (P8.IsValidAmount(this, editAmount)) {
			mAmount = Integer.parseInt(editAmount.getText().toString());
			if(mAmount == 0) {
				P8.ErrorSound(this);
				editAmount.setError(getString(R.string.error_zero_number));
				editAmount.requestFocus();
			}
			else {
				if(!BalanceEqual(cl.get_LB(), getString(R.string.description_loan), null, getString(R.string.description_repay)))
					VerifyAmount(getString(R.string.description_repay), null);
			}
		}
	}
	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#UpdateTransactions()
	 */
	@Override
	protected boolean UpdateTransactions() {
		// TODO Auto-generated method stub
		if(super.UpdateTransactions()) {
			ShowBanglaBalance("GLb FY cwi‡kva", mAmount, "†gvU cwi‡kvwaZ FY", cl.get_TRB());
			finish();
		}
		return false;
	}

	public void onReport(View view) {
	  Intent it = new Intent(this, MLCReportActivity.class);
	  it.putExtra(P8.LOANNO, cl.get_LoanNo());
	  startActivity(it);
	}

	public void calcFutureBalance(View view) {
		fromDatePickerDialog.show();
	}
}
