package net.qsoft.brac.bmsmerp;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;
import net.qsoft.brac.bmsmerp.data.PO;

import java.util.ArrayList;
import java.util.HashMap;

public class CListSummaryActivity extends SSActivity {
    private static final String TAG = CListSummaryActivity.class.getSimpleName();

    ListView lv = null;
    TextView branchName;
    TextView coName;

    ArrayList<HashMap<String, String>> items=null;
    Long vono=null;
    String pono=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clist_summary);

        Button cmdOK = (Button) findViewById(R.id.okButton);
        cmdOK.setVisibility(View.GONE);

        branchName = (TextView) findViewById(R.id.textBranchName);
        coName = (TextView) findViewById(R.id.textCOName);

        lv = (ListView) findViewById(R.id.listViewCLS);

        if(getIntent().hasExtra(P8.VONO)) {
            vono = getIntent().getExtras().getLong(P8.VONO);
//			Log.d(TAG, "In has extra " + vono);
            setTitle("Collec. Sumamry VO No.: " + vono);
        }
        else if(getIntent().hasExtra(P8.PONO)) {
            pono = getIntent().getExtras().getString(P8.PONO);
            setTitle("Collec. Sumamry CO/PO No.: " + pono);
        } else {
            setTitle("Collection Details");
        }
//        getSupportActionBar().setTitle(getTitle());

        createCollectionSummary(vono, pono);
    }

    private void createCollectionSummary(Long von, String pon) {
//		Log.d(TAG, "VO No.: " + (von==null ? "": von));

        DAO da = new DAO(this);
        da.open();
        items = da.getCollectionSummary(von, pon);
        PO po = da.getPO();
        CO co = da.getCO(pon);
        da.close();

        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
        coName.setText(pon + " - " + co.get_COName());

        String[] from = new String[] {DBHelper.FLD_ORG_NO, DBHelper.FLD_ORG_NAME, DBHelper.FLD_LOAN_STATUS_NAME, "AMOUNT"};
        int[] to = { R.id.textOrgNo, R.id.textOrgName, R.id.textStatus, R.id.textAmount };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.clist_summary_row, from, to){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById( R.id.textOrgNo);
                if(text.getText().equals("Grand total"))
                    view.setBackgroundColor(getResources().getColor(R.color.Coral));
                else if(text.getText().equals("Total"))
                    view.setBackgroundColor(getResources().getColor(R.color.Lavender));
                else
                    view.setBackgroundColor(getResources().getColor(R.color.report_background));
                return view;
            }
        };

        lv.setAdapter(adapter);
    }

    public void onCancel(View view) {
        // Back
        finish();
    }
}
