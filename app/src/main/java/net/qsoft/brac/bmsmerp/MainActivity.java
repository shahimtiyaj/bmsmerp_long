package net.qsoft.brac.bmsmerp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.qsoft.brac.bmsm.BuildConfig;
import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.activity.CDuringGracePeriodActivity;
import net.qsoft.brac.bmsmerp.activity.CurrentInstallmentActivity;
import net.qsoft.brac.bmsmerp.activity.CurrentScheduleActivity;
import net.qsoft.brac.bmsmerp.activity.LoanInGracePeriodActivity;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.PO;
import net.qsoft.brac.bmsmerp.util.CloudRequest;
import net.qsoft.brac.bmsmerp.util.SMSListener;

import java.util.HashMap;

import static net.qsoft.brac.bmsmerp.App.getSMSSendURL;

public class MainActivity extends SSActivity implements SMSListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    ImageButton btnDashboard, btnOperation, btnPrr, btnAdmission, btnReports, btnExit;
    private static final int REQUEST_SIGNUP = 0;

    TextView textSyncInfo;
    TextView collLabel;
    TextView branchLabel;
    TextView dlStatusLabel;
    TextView textVersion;
    TextView textViewOk;

    RelativeLayout btn_menu_layout;
    boolean startMenuEnabled = false;
    PO coll;

    //private BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        viewInitialization();
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckDataState();
     /* IntentFilter intentFilter = new IntentFilter("com.example.UPLOAD_FINISHED");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(getApplicationContext(),"Updating Done !",Toast.LENGTH_LONG).show();
            }
        };
        registerReceiver(broadcastReceiver, intentFilter); */
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(broadcastReceiver);
    }

    private void CheckDataState() {
        DAO data = new DAO(this);
        data.open();
        coll = data.getPO();

        App.setBranchOPenDate(coll.get_BranchOpenDate());
        App.setLastSyncDate(P8.getLastSyncTime());
        if (coll.get_SMSSupport() > 0)
            App.setSMSActivity(this);
        else
            App.setSMSActivity(null);

        if (coll.isPO()) {
            //Log.d(TAG, "Fro CheckDataState - after if coll.isPO()");

            collLabel.setText(coll.toString());

            Log.d(TAG, App.getLastSyncGaps());
            textSyncInfo.setText(App.getLastSyncGaps());
            if (coll.get_BranchCode().equals("0000"))
                branchLabel.setText("");
            else
                branchLabel.setText(coll.get_BranchCode() + " - " + coll.get_BranchName());
            branchLabel.setVisibility(View.VISIBLE);
            //dlStatusLabel.setVisibility(View.VISIBLE);
            //dashboard.setEnabled(true);
            startMenuEnabled = true;
            //btn_menu_layout.setEnabled(true);
            btn_menu_layout.setVisibility(View.VISIBLE);

            if (coll.get_EMethod() == PO.EM_DESKTOP) {
                textSyncInfo.setVisibility(View.INVISIBLE);
                dlStatusLabel.setVisibility(View.VISIBLE);
            } else {
                textSyncInfo.setVisibility(View.VISIBLE);
                dlStatusLabel.setVisibility(View.INVISIBLE);
            }
        } else {
            //	Log.d(TAG, "Fro CheckDataState - after else coll.isPO()");
            branchLabel.setVisibility(View.INVISIBLE);
            // dlStatusLabel.setVisibility(View.INVISIBLE);

            //dashboard.setEnabled(false);
            startMenuEnabled = false;
            btn_menu_layout.setVisibility(View.INVISIBLE);
            //) .setEnabled(false);
        }

        Log.d(TAG, "From CheckDataState - after if(isPO) block");
        data.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        super.onPrepareOptionsMenu(menu);
        //  menu.getItem(0).setEnabled(startMenuEnabled);
        return true;
    }

    public void deviceIdAlert() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_perm_device_information_black_24dp)
                .setTitle("Device ID")
                .setMessage(App.getDeviceId())
                .setNegativeButton("Ok", null)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            /*
            case R.id.menu_start:
                // Start next activity - collector password
                Startup(null);
                return true; */

            case R.id.menu_syncronize:
                // Synchronize data

                Intent intent = new Intent(getApplicationContext(), SyncDataActivity.class);
               // Intent intent = new Intent(getApplicationContext(), SyncDataSimpleJsonParser_1.class);
                startActivity(intent);

                return true;

            case R.id.menu_settings:
                startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
                return true;

            case R.id.menu_device_id:
                deviceIdAlert();
                return true;
            case R.id.menu_datetime:
                startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
                return true;

           /* case R.id.register:
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                return true;*/

            case R.id.menu_power:
                startActivity(new Intent("android.intent.action.POWER_USAGE_SUMMARY"));
                return true;

            case R.id.menu_exit:
                ExitApp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void Startup(View view) {
        // Button onClick - start activity - collector password
        if (coll.get_Password().length() > 0) {
            Intent it = new Intent(this, LoginActivity.class);
            it.putExtra(LoginActivity.EXTRA_PASS, coll.get_Password());
            startActivityForResult(it, 101);    // R.id.menu_start'
        } else {
            Intent it = new Intent(this, TTActivity.class);
            startActivity(it);
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void ExitApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Exit BMSM");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to quit BMSM?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close current activity
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory(Intent.CATEGORY_HOME);
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        // MainActivity.this.finish();
                        // System.exit(1);
                        // android.os.Process.killProcess(android.os.Process.myPid());
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void Reports(View v) {
        startActivity(new Intent(this, ReportsMenuActivity.class));
        if (true) return;

        PopupMenu popup = new PopupMenu(MainActivity.this, v);

        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.reports_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_rept_overdue) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 201); // R.id.menu_rept_overdue
                } else if (item.getItemId() == R.id.menu_rept_vo_loan_disb) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 202); // R.id.menu_rept_vo_loan_disb
                } else if (item.getItemId() == R.id.menu_rept_good_loan) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 203); // R.id.menu_rept_good_loan
                } else if (item.getItemId() == R.id.menu_rept_loan_target_vs_achive) {
                    startActivity(new Intent(App.getContext(), LRTAReportActivity.class));
                } else if (item.getItemId() == R.id.menu_rept_current_loan_adjustment) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 204); // R.id.menu_rept_current_loan_adjustment
                } else if (item.getItemId() == R.id.menu_rept_collection_summary) {
                    startActivityForResult(new Intent(App.getContext(), COResultActivity.class), 205); // R.id.menu_rept_collection_summary
                } else if (item.getItemId() == R.id.menu_rep_loan_collection_summary) {
                    startActivity(new Intent(App.getContext(), LCReportBranchSum.class)); // R.id.menu_rep_loan_collection_summary
                } else if (item.getItemId() == R.id.menu_cins_miss_borr) {
                    startActivity(new Intent(App.getContext(), CurrentInstallmentActivity.class));
                } else if (item.getItemId() == R.id.menu_csmissed_borr) {
                    startActivity(new Intent(App.getContext(), CurrentScheduleActivity.class));
                } else if (item.getItemId() == R.id.menu_cdgrace_period) {
                    startActivity(new Intent(App.getContext(), CDuringGracePeriodActivity.class));
                } else if (item.getItemId() == R.id.menu_loann_in_grac_period) {
                    startActivity(new Intent(App.getContext(), LoanInGracePeriodActivity.class));
                }

                //Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();
    }

    public void OperationsMgt(View v) {
        PopupMenu popup = new PopupMenu(MainActivity.this, v);

        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.operations_menu, popup.getMenu());
        popup.getMenu().getItem(2).setVisible(App.hasSMSSupport());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_oprtns_collection) {
                    Intent intent = new Intent(getApplicationContext(), VOActivity.class);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.menu_oprtns_syncstat) {
                    Intent intent = new Intent(getApplicationContext(), COSyncActivity.class);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.menu_oprtns_smsstat) {
                    Intent intent = new Intent(getApplicationContext(), SMSReportActivity.class);
                    startActivity(intent);
                }

                return true;
            }
        });
        popup.show();
    }

    public void PrrOption(View v) {
        PopupMenu popup = new PopupMenu(MainActivity.this, v);

        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.prr_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_prr_field_visit) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 301);  // R.id.menu_prr_field_visit

                } else if (item.getItemId() == R.id.menu_prr_followup) {
                    startActivity(new Intent(App.getContext(), FollowupActivity.class));

                } else if (item.getItemId() == R.id.menu_rept_pbcheck_by_org) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), 207); // R.id.menu_rept_pbcheck_by_org

                } else if (item.getItemId() == R.id.menu_rept_cscheck_info) {
                    startActivity(new Intent(App.getContext(), CSCheckInfo.class));
                } else if (item.getItemId() == R.id.menu_rept_loan_disburs_info) {
                    startActivity(new Intent(App.getContext(), LoanDisbursInfo.class));
                }

                return true;
            }
        });
        popup.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

           /* case 101:
            case (R.id.menu_start): {
                if (resultCode == Activity.RESULT_OK) {
                    // Start transaction options Activity
                    Intent it = new Intent(this, TTActivity.class);
                    startActivity(it);
                }
            }
            break; */

            case 201:
            case (R.id.menu_rept_overdue): {
                if (resultCode == Activity.RESULT_OK) {
                    // Overdue report activity
                    Intent it = new Intent(this, ODReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    // Log.d(TAG, "VO No. returned: " + vn);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 202:
            case (R.id.menu_rept_vo_loan_disb): {
                // VO-wise loan disbursement report
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, VLDReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 203:
            case (R.id.menu_rept_good_loan): {
                // Good loan list report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, GoodLoanListActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 204:
            case (R.id.menu_rept_current_loan_adjustment): {
                // Current loan adjustment report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CLAReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 205:
            case (R.id.menu_rept_collection_summary): {
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CListSummaryActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 207:
            case (R.id.menu_rept_pbcheck_by_org): {
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, PBCheckReportByVO.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 301:
            case (R.id.menu_prr_field_visit): {
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, PBCheckActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

        }
    }

    @Override
    public void SendSMS(String branch_code, String project_code, String po_no, String orgno, String orgmemno, String mobile, String message, final String records) {

        String api_key = "i2yRuU6yspWf7NDsrFdkz7vlMrz35SDZ";

        //Send SMS API
        String mainUrl = getSMSSendURL();

        HashMap<String, String> mParams = new HashMap<String, String>();
        mParams.put("mobile", mobile.trim());
        mParams.put("branch_code", branch_code);
        mParams.put("orgno", orgno);
        mParams.put("orgmemno", orgmemno);
        mParams.put("message", message);
        mParams.put("po_no", po_no);
        mParams.put("project_code", project_code);
        mParams.put("api_key", api_key);

        final HashMap<String, String> mp = mParams;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // SMS sent update database
                        String resp = Html.fromHtml(response).toString().trim();
                        String[] st = resp.split(",");
                        if (st[0].trim().equals("200")) {
                            if (records.trim().length() > 0) {
                                DAO da = new DAO(MainActivity.this);
                                da.open();
                                da.ConfirmSMSSent(records);
                                da.close();
                            }
                        } else {
                            P8.ShowError(MainActivity.this, resp);
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                P8.ShowError(MainActivity.this, VolleyErrorResponse(error));
            }
        }) {
            @Override
            public HashMap<String, String> getParams() {
                return mp;
            }
        };

        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(stringRequest);
    }

    public static String VolleyErrorResponse(VolleyError volleyError) {
        // TODO Auto-generated method stub
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        } else {
            message = volleyError.toString();
        }

        return message;
    }

    public void viewInitialization() {

        textSyncInfo = (TextView) findViewById(R.id.textSyncInfo);
        collLabel = (TextView) findViewById(R.id.collLabel);
        branchLabel = (TextView) findViewById(R.id.branchLabel);
        dlStatusLabel = (TextView) findViewById(R.id.dlStatuslabel);
        textVersion = (TextView) findViewById(R.id.textVersion);
        textViewOk = (TextView) findViewById(R.id.textOK);

        btn_menu_layout = (RelativeLayout) findViewById(R.id.btn_menu_layout);
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        textVersion.setText("BMFPO Version " + versionName);

        btnDashboard = (ImageButton) findViewById(R.id.dashboard);
        btnOperation = (ImageButton) findViewById(R.id.operation);
        btnPrr = (ImageButton) findViewById(R.id.prr);
        btnReports = (ImageButton) findViewById(R.id.reports);
        btnAdmission = (ImageButton) findViewById(R.id.admission);
        btnExit = (ImageButton) findViewById(R.id.exit);

//        Intent intent = getIntent();
//        int name = intent.getIntExtra("ok", 0);
//        if (name == 2) {
//            textViewOk.setVisibility(View.GONE);
//            DeleteBMzipFile();
//            DeleteBMjsonFile();
//            DeleteTransJsonFile();
//        } else if ((name == 1)) {
//            textViewOk.setText("Processing ...");
//        }

        btnDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TTActivity.class);
                startActivity(intent);
            }
        });

        btnOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperationsMgt(v);
            }
        });

        btnPrr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrrOption(v);

            }
        });

        btnAdmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                P8.NotImplemented();

            }
        });

        btnReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reports(v);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExitApp();
            }
        });

    }
}
