package net.qsoft.brac.bmsmerp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.activity.CDuringGracePeriodActivity;
import net.qsoft.brac.bmsmerp.activity.CurrentInstallmentActivity;
import net.qsoft.brac.bmsmerp.activity.CurrentScheduleActivity;
import net.qsoft.brac.bmsmerp.activity.LoanInGracePeriodActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportsMenuActivity extends SSActivity {
    private static final String TAG = ReportsMenuActivity.class.getSimpleName();

    @BindView(R.id.lstView) ListView lv = null;

    //ArrayList<String> items=new ArrayList<String>();
    String [] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_colist);
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        items = getResources().getStringArray(R.array.reports_menu);

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, items);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String x = lv.getItemAtPosition(position).toString();
                Integer i = Integer.parseInt(x.substring(0, x.indexOf(".")).trim());

                switch (i) {
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                        startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), i);
                        break;

                    case 4:
                        startActivity(new Intent(App.getContext(),LRTAReportActivity.class));
                        break;

                    case 6:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        startActivityForResult(new Intent(App.getContext(), COResultActivity.class), i);
                        break;

                    case 7:
                        startActivity(new Intent(App.getContext(), LCReportBranchSum.class));
                        break;
                }
//                    Toast.makeText(getBaseContext(), x, Toast.LENGTH_LONG).show();
            }
        });

        lv.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    // Overdue report activity
                    Intent it = new Intent(this, ODReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
//	    		    Log.d(TAG, "VO No. returned: " + vn);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
                break;

            case 2:
                // VO-wise loan disbursement report
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, VLDReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
                break;

            case 3:
                // Good loan list report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, GoodLoanListActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
                break;

            case 5:
                // Current loan adjustment report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CLAReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO,0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
                break;

            case 6:
                // All Collection Summary
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CListSummaryActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
                break;

            case 8:
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CurrentInstallmentActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
                break;

            case 9:
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CurrentScheduleActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
                break;

            case 10:
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, CDuringGracePeriodActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
                break;

            case 11:
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, LoanInGracePeriodActivity.class);
                    String vn = data.getStringExtra(P8.PONO);
                    it.putExtra(P8.PONO, vn);
                    startActivity(it);
                }
                break;

/*            case 207:
            case (R.id.menu_rept_pbcheck_by_org): {
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, PBCheckReportByVO.class);
                    String vn = data.getStringExtra(P8.VONO);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case 301:
            case (R.id.menu_prr_field_visit): {
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, PBCheckActivity.class);
                    String vn = data.getStringExtra(P8.VONO);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;
*/
        }
    }


    public void onCancel(View view) {
        // Back
        finish();
    }
}
