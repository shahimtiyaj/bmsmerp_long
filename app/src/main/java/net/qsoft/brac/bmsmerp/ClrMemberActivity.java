package net.qsoft.brac.bmsmerp;

import java.util.ArrayList;
import java.util.HashMap;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ClrMemberActivity extends SSActivity {
	private static final String TAG = ClrMemberActivity.class.getSimpleName();
	
	ArrayList<HashMap<String, String>> memlist=null;
	ClrMemListAdapter adapter;
	EditText inputSearch=null;
	ListView lv=null;
	DAO.DBoardItem itm=null;
	Long vono=null;
	String pono=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_member);

		inputSearch = (EditText) findViewById(R.id.inputSearch);
		lv = (ListView) findViewById(R.id.lstView);
		
		setupList();

	    adapter = new ClrMemListAdapter(getApplicationContext(), memlist);
	    lv.setAdapter(adapter);

	    lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
            	Long vono = Long.valueOf(x.get(DBHelper.FLD_ORG_NO));
            	Long memno = Long.valueOf(x.get(DBHelper.FLD_ORG_MEM_NO));
            	Intent it = new Intent(getApplicationContext(), MempfActivity.class);
            	it.putExtra(P8.VONO, vono);
            	it.putExtra(P8.MEMNO, memno);
            	startActivity(it);
            	
//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });
		
        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				ClrMemberActivity.this.adapter.getFilter().filter(s);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	        
	} 
	
	protected void setupList() {

		Intent it = getIntent();
		if(it.hasExtra(P8.DBITEMS)) {
			itm = DAO.DBoardItem.valueOf(it.getStringExtra(P8.DBITEMS));
		}
		if(it.hasExtra(P8.VONO))
			vono = it.getLongExtra(P8.VONO,0);
		else if(it.hasExtra(P8.PONO))
			pono = it.getStringExtra(P8.PONO);
//		Log.d(TAG, "DBItem: " + itm.toString());
//		Log.d(TAG, "VONo.: " + vono);
		
		DAO da = new DAO(this);
		da.open();
		memlist = da.getMemberList(itm, vono, pono);
		da.close();

	}

	public void onCancel(View view) {
		// Back
		finish();
	}		
}
