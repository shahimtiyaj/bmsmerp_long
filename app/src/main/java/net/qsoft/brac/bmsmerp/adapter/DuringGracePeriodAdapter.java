package net.qsoft.brac.bmsmerp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.CDGPModel;

import java.util.ArrayList;

public class DuringGracePeriodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<CDGPModel> duringGracePeriodArrayList;
    private Context context;

    public DuringGracePeriodAdapter(Context context, ArrayList<CDGPModel> duringGraceArrayList) {
        this.context = context;
        this.duringGracePeriodArrayList = duringGraceArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_during_grace_period, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_HEADER) {
            //Inflating header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.total_amount, parent, false);
            return new HeaderViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_footer_layout, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            // headerHolder.headerTitle.setText("Header View");

        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.footerText.setText("Footer View");

        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder viewHolder = (ItemViewHolder) holder;

            final CDGPModel row = (CDGPModel) duringGracePeriodArrayList.get(position);

            if(duringGracePeriodArrayList.size() == (position + 1)) {
                viewHolder.si_no.setText("");
                viewHolder.st_name.setText("");
                viewHolder.vo_code.setText("");
                viewHolder.member_code.setText("");
                viewHolder.member_name.setText("");
                viewHolder.loan_number.setText("");
                viewHolder.target_date.setText("Total:");
                viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                viewHolder.coll_amount.setText(String.format("%,d", row.getCollAmount()));
                viewHolder.mobile_no.setText("");

                viewHolder.target_date.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.target_ammount.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.coll_amount.setTypeface(Typeface.DEFAULT_BOLD);
                viewHolder.itemView.setBackgroundResource(R.drawable.footer_border);
            }
            else {
                viewHolder.si_no.setText("" + (position + 1));
                viewHolder.st_name.setText(row.getTerritoryName());
                viewHolder.vo_code.setText(row.getOrgNo());
                viewHolder.member_code.setText(row.getOrgMemNo());
                viewHolder.member_name.setText(row.getMemberName());
                viewHolder.loan_number.setText(row.getLoanNo());
                viewHolder.target_date.setText(row.getTargetDate("dd-MMM-yyyy"));
                viewHolder.target_ammount.setText(String.format("%,d", row.getTargetAmount()));
                viewHolder.coll_amount.setText(String.format("%,d", row.getCollAmount()));
                viewHolder.mobile_no.setText(row.getPhoneNo());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == duringGracePeriodArrayList.size() + 1) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;

        }
    }

    @Override
    public int getItemCount() {
        return duringGracePeriodArrayList.size();
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTitle;

        public HeaderViewHolder(View view) {
            super(view);
            headerTitle = (TextView) view.findViewById(R.id.totalSum);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView si_no, st_name, vo_code, member_code, member_name, loan_number,
                target_date, target_ammount, coll_amount, mobile_no;

        public ItemViewHolder(View itemView) {
            super(itemView);

            si_no = itemView.findViewById(R.id.CDGracetextSIno);
            st_name = itemView.findViewById(R.id.CDGracetextSTName);
            vo_code = itemView.findViewById(R.id.CDGracetextVOCode);
            member_code = itemView.findViewById(R.id.CDGracetextMemberCode);
            member_name = itemView.findViewById(R.id.CDGracetextMemberName);
            loan_number = itemView.findViewById(R.id.CDGracetextLoanNumber);
            target_date = itemView.findViewById(R.id.CDGracetextTargetdate);
            target_ammount = itemView.findViewById(R.id.CDGracetextTargetAmount);
            coll_amount = itemView.findViewById(R.id.CDGracetextCollectionAmount);
            mobile_no = itemView.findViewById(R.id.CDGracetextMobileNo);
        }
    }
}
