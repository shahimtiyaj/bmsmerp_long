package net.qsoft.brac.bmsmerp.util;

import android.os.AsyncTask;

import net.qsoft.brac.bmsmerp.App;
import net.qsoft.brac.bmsmerp.P8;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import static net.qsoft.brac.bmsmerp.App.getBaseURL;

/**
 * Created by zferdaus on 8/26/2017.
 */

public class SMSServer {
    private static final String TAG = SMSServer.class.getSimpleName();

    public void SendSMS(String branch_code, String project_code,
                               String po_no, String orgno, String orgmemno, String mobile, String message ) {

        String api_key = "i2yRuU6yspWf7NDsrFdkz7vlMrz35SDZ";
        //encoding message
        String encoded_message=  URLEncoder.encode(message);

        //Send SMS API
        String mainUrl=getBaseURL() + "/sms/send.php";

        //Prepare parameter string
        StringBuilder sbPostData= new StringBuilder();
        sbPostData.append("mobile="+mobile);
        sbPostData.append("&branch_code="+branch_code);
        sbPostData.append("&orgno="+orgno);
        sbPostData.append("&orgmemno="+orgmemno);
        sbPostData.append("&message="+encoded_message);
        sbPostData.append("&po_no="+po_no);
        sbPostData.append("&project_code="+project_code);
        sbPostData.append("&api_key="+api_key);
        doPost bk = new doPost();
        bk.execute(mainUrl, sbPostData.toString());
    }

    private class doPost extends AsyncTask<String, Void, String> {
        private String errMsg="";
        String resp="";
        @Override
        protected String doInBackground(String... params) {
            errMsg="";
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            HttpURLConnection conn = null;
            byte[] buf = new byte[4096];
            try {
                URL url = new URL(params[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                String body = params[1];
                OutputStream output = new BufferedOutputStream(conn.getOutputStream());
                output.write(body.getBytes());
                output.flush();

                //This is needed
                // Could alternatively use conn.getResponseMessage() or conn.getInputStream()
                //conn.getResponseCode();
                resp = conn.getResponseMessage();

                InputStream is = conn.getInputStream();
                int ret = 0;
                while ((ret = is.read(buf)) > 0) {
                    os.write(buf, 0, ret);
                }
                // close the inputstream
                is.close();
                resp = new String(os.toByteArray());

            } catch (ProtocolException e) {
               // P8.ShowError(App.getContext(), e.getMessage());
                errMsg = e.getLocalizedMessage();
                //e.printStackTrace();
            } catch (IOException e) {
              //  P8.ShowError(App.getContext(), e.getMessage());
                errMsg = e.getLocalizedMessage();
                //e.printStackTrace();
            } finally {
                conn.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aLong) {
            super.onPostExecute(aLong);

            if(!errMsg.isEmpty())
                P8.ShowError(App.getContext(), errMsg);
        }
    }
}