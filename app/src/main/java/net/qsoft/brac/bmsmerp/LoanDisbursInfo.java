package net.qsoft.brac.bmsmerp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.DBHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class LoanDisbursInfo extends SSActivity {
    private static final String TAG = LoanDisbursInfo.class.getSimpleName();

    TextView mDate;
    ListView lv;

    Date datDate;
    private DatePickerDialog fromDatePickerDialog;

    ArrayList<HashMap<String, String>> itms = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_disburs_info);

        Button okButton = (Button) findViewById(R.id.okButton);
        okButton.setVisibility(View.GONE);

        mDate = (TextView) findViewById(R.id.textDate);
        lv = (ListView) findViewById(R.id.listViewLDInfo);

        datDate = P8.ToDay();
        Calendar newCal = Calendar.getInstance();
        newCal.setTime(datDate);

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                datDate = c.getTime();
                ShowReport();
            }
        }, newCal.get(Calendar.YEAR), newCal.get(Calendar.MONTH), newCal.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowReport();
    }

    private void ShowReport() {
        mDate.setText(P8.FormatDate(datDate, "dd-MMM-yyyy"));

        DAO da = new DAO(this);
        da.open();
        itms = da.getLoanDisbursInfoReport(datDate);
        da.close();

        String[] from = new String[]{DBHelper.FLD_LOAN_TYPE, DBHelper.FLD_TONUMB, DBHelper.FLD_TOAMT,
                DBHelper.FLD_CUMILIT_NO, DBHelper.FLD_CUMILIT_AMT};
        int[] to = {R.id.textLoanType, R.id.textTodayNum, R.id.textTodayAmt, R.id.textCumNum, R.id.textCumAmt};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), itms, R.layout.loan_disburs_info_row, from, to);
        lv.setAdapter(adapter);
    }

    public void onDateClick(View v) {
        if (v.getId() == R.id.textDate)
            fromDatePickerDialog.show();
    }

    public void onCancel(View v) {
        finish();
    }

}