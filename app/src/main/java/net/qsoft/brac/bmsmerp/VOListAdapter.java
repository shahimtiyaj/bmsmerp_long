package net.qsoft.brac.bmsmerp;
import java.util.ArrayList;
import java.util.TreeSet;

import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.data.VO;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class VOListAdapter extends BaseAdapter {

	private static final int TYPE_ITEM = 0;
	private static final int TYPE_SEPARATOR = 1;

	private ArrayList<DataHolder> mData = new ArrayList<DataHolder>();
	private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

	private LayoutInflater mInflater;

	public VOListAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void addItem(final VO item) {
		DataHolder x = new DataHolder();
		x.vo=item;

		if(item.get_TerritoryName().trim().length() > 0)
			x.Label = item.get_OrgNo() + " - " + item.get_OrgName() + "\r\n" + item.get_TerritoryName().trim();
		else
			x.Label = item.get_OrgNo() + " - " + item.get_OrgName();

		x.COName = item.get_COName();
		x.Type = TYPE_ITEM;
		mData.add(x);
		notifyDataSetChanged();
	}

	public void addSectionHeaderItem(final String item) {
		DataHolder x = new DataHolder();
		x.vo=null;
		x.Label = item;
		x.COName = "";
		x.Type=TYPE_SEPARATOR;
		mData.add(x);
		sectionHeader.add(mData.size() - 1);
		notifyDataSetChanged();
	}

	@Override
	public int getItemViewType(int position) {
		return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public DataHolder getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		int rowType = getItemViewType(position);

		if (convertView == null) {
			holder = new ViewHolder();
			switch (rowType) {
			case TYPE_ITEM:
				convertView = mInflater.inflate(R.layout.snippet_item1, null);
				holder.textView = (TextView) convertView.findViewById(R.id.text);
				holder.textView2 = (TextView) convertView.findViewById(R.id.text2);
				break;
				
			case TYPE_SEPARATOR:
				convertView = mInflater.inflate(R.layout.snippet_item2, null);
				holder.textView = (TextView) convertView.findViewById(R.id.textSeparator);
				convertView.setOnClickListener(null);
				convertView.setLongClickable(false);
				break;
			}
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.textView.setText(mData.get(position).Label);
		if(rowType==TYPE_ITEM)
			holder.textView2.setText(mData.get(position).COName);

		return convertView;
	}

	public static class ViewHolder {
		public TextView textView;
		public TextView textView2;
	}
	
	public static class DataHolder {
		VO vo;
		String Label;
		Integer Type;
		String COName;
	}
}