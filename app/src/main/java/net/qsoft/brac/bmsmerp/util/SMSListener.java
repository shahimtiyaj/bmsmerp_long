package net.qsoft.brac.bmsmerp.util;

/**
 * Created by zferdaus on 8/27/2017.
 */

public interface SMSListener {
    public void SendSMS(String branch_code, String project_code,
                        String po_no, String orgno, String orgmemno, String mobile, String message, String records );
}
