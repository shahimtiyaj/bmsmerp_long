package net.qsoft.brac.bmsmerp.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.qsoft.brac.bmsmerp.App;
import net.qsoft.brac.bmsmerp.P8;
import net.qsoft.brac.bmsm.R;
import net.qsoft.brac.bmsmerp.SSActivity;
import net.qsoft.brac.bmsmerp.adapter.DuringGracePeriodAdapter;
import net.qsoft.brac.bmsmerp.data.CDGPModel;
import net.qsoft.brac.bmsmerp.data.CO;
import net.qsoft.brac.bmsmerp.data.DAO;
import net.qsoft.brac.bmsmerp.data.PO;

import java.util.ArrayList;

public class CDuringGracePeriodActivity extends SSActivity {
    private static final String TAG = CDuringGracePeriodActivity.class.getSimpleName();

    private LinearLayoutManager layoutManager;
    private DuringGracePeriodAdapter adapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private ArrayList<CDGPModel> duringGraceArrayList;
    private RelativeLayout mainLayout, emptyLayout;
    private Button cmdOK;
    private TextView branchName, CInsMissBorrtotalTargetSum, voName;

    private String pono = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.during_grace_period_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        cmdOK = (Button) findViewById(R.id.okButton);
        ((Button) findViewById(R.id.cancelButton)).setVisibility(View.GONE);

        branchName = (TextView) findViewById(R.id.textBranchName);

        if(getIntent().hasExtra(P8.PONO)) {
            pono = getIntent().getExtras().getString(P8.PONO);
        }

        duringGraceArrayList = new ArrayList<>();
        adapter = new DuringGracePeriodAdapter(this, duringGraceArrayList);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.during_grace_list_recycler_view);
        // swipeRefresh = findViewById(R.id.swipe_refresh_layout);
        mainLayout = findViewById(R.id.main_layout);
//        emptyLayout = findViewById(R.id.empty_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        loadCurrentSchedule();
    }

    private void loadCurrentSchedule() {
        DAO dao = new DAO(App.getContext());
        dao.open();
        PO po = dao.getPO();
        duringGraceArrayList = dao.getDuringGracePeriodData(pono);
        CO co = dao.getCO(pono);
        dao.close();

        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName()
                + "\n" + co.get_CONo() + " - " + co.get_COName());

//        if (!duringGraceArrayList.isEmpty()) {
            adapter = new DuringGracePeriodAdapter(this, duringGraceArrayList);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(adapter);
            // swipeRefresh.setRefreshing(false);
//            emptyLayout.setVisibility(View.GONE);
//        } else {
//            emptyLayout.setVisibility(View.VISIBLE);
//            //  swipeRefresh.setRefreshing(false);
//        }
//        dao.close();
    }

    public void onOk(View view) {
        finish();
    }
}
